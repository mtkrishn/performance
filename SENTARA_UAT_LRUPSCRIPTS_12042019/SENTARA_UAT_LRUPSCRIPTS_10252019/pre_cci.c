# 1 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c"
# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h" 1
 
 












 











# 103 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"






















































		


		typedef unsigned size_t;
	
	
        
	

















	

 



















 
 
 
 
 


 
 
 
 
 
 














int     lr_start_transaction   (char * transaction_name);
int lr_start_sub_transaction          (char * transaction_name, char * trans_parent);
long lr_start_transaction_instance    (char * transaction_name, long parent_handle);
int   lr_start_cross_vuser_transaction		(char * transaction_name, char * trans_id_param); 



int     lr_end_transaction     (char * transaction_name, int status);
int lr_end_sub_transaction            (char * transaction_name, int status);
int lr_end_transaction_instance       (long transaction, int status);
int   lr_end_cross_vuser_transaction	(char * transaction_name, char * trans_id_param, int status);


 
typedef char* lr_uuid_t;
 



lr_uuid_t lr_generate_uuid();

 


int lr_generate_uuid_free(lr_uuid_t uuid);

 



int lr_generate_uuid_on_buf(lr_uuid_t buf);

   
# 273 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int lr_start_distributed_transaction  (char * transaction_name, lr_uuid_t correlator, long timeout  );

   







int lr_end_distributed_transaction  (lr_uuid_t correlator, int status);


double lr_stop_transaction            (char * transaction_name);
double lr_stop_transaction_instance   (long parent_handle);


void lr_resume_transaction           (char * trans_name);
void lr_resume_transaction_instance  (long trans_handle);


int lr_update_transaction            (const char *trans_name);


 
void lr_wasted_time(long time);


 
int lr_set_transaction(const char *name, double duration, int status);
 
long lr_set_transaction_instance(const char *name, double duration, int status, long parent_handle);


int   lr_user_data_point                      (char *, double);
long lr_user_data_point_instance                   (char *, double, long);
 



int lr_user_data_point_ex(const char *dp_name, double value, int log_flag);
long lr_user_data_point_instance_ex(const char *dp_name, double value, long parent_handle, int log_flag);


int lr_transaction_add_info      (const char *trans_name, char *info);
int lr_transaction_instance_add_info   (long trans_handle, char *info);
int lr_dpoint_add_info           (const char *dpoint_name, char *info);
int lr_dpoint_instance_add_info        (long dpoint_handle, char *info);


double lr_get_transaction_duration       (char * trans_name);
double lr_get_trans_instance_duration    (long trans_handle);
double lr_get_transaction_think_time     (char * trans_name);
double lr_get_trans_instance_think_time  (long trans_handle);
double lr_get_transaction_wasted_time    (char * trans_name);
double lr_get_trans_instance_wasted_time (long trans_handle);
int    lr_get_transaction_status		 (char * trans_name);
int	   lr_get_trans_instance_status		 (long trans_handle);

 



int lr_set_transaction_status(int status);

 



int lr_set_transaction_status_by_name(int status, const char *trans_name);
int lr_set_transaction_instance_status(int status, long trans_handle);


typedef void* merc_timer_handle_t;
 

merc_timer_handle_t lr_start_timer();
double lr_end_timer(merc_timer_handle_t timer_handle);


 
 
 
 
 
 











 



int   lr_rendezvous  (char * rendezvous_name);
 




int   lr_rendezvous_ex (char * rendezvous_name);



 
 
 
 
 
char *lr_get_vuser_ip (void);
void   lr_whoami (int *vuser_id, char ** sgroup, int *scid);
char *	  lr_get_host_name (void);
char *	  lr_get_master_host_name (void);

 
long     lr_get_attrib_long	(char * attr_name);
char *   lr_get_attrib_string	(char * attr_name);
double   lr_get_attrib_double      (char * attr_name);

char * lr_paramarr_idx(const char * paramArrayName, unsigned int index);
char * lr_paramarr_random(const char * paramArrayName);
int    lr_paramarr_len(const char * paramArrayName);

int	lr_param_unique(const char * paramName);
int lr_param_sprintf(const char * paramName, const char * format, ...);


 
 
static void *ci_this_context = 0;






 








void lr_continue_on_error (int lr_continue);
char *   lr_decrypt (const char *EncodedString);


 
 
 
 
 
 



 







 















void   lr_abort (void);
void lr_exit(int exit_option, int exit_status);
void lr_abort_ex (unsigned long flags);

void   lr_peek_events (void);


 
 
 
 
 


void   lr_think_time (double secs);

 


void lr_force_think_time (double secs);


 
 
 
 
 



















int   lr_msg (char * fmt, ...);
int   lr_debug_message (unsigned int msg_class,
									    char * format,
										...);
# 512 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_new_prefix (int type,
                                 char * filename,
                                 int line);
# 515 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_log_message (char * fmt, ...);
int   lr_message (char * fmt, ...);
int   lr_error_message (char * fmt, ...);
int   lr_output_message (char * fmt, ...);
int   lr_vuser_status_message (char * fmt, ...);
int   lr_error_message_without_fileline (char * fmt, ...);
int   lr_fail_trans_with_error (char * fmt, ...);

 
 
 
 
 
# 539 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

 
 
 
 
 





int   lr_next_row ( char * table);
int lr_advance_param ( char * param);



														  
														  

														  
														  

													      
 


char *   lr_eval_string (char * str);
int   lr_eval_string_ext (const char *in_str,
                                     unsigned long const in_len,
                                     char ** const out_str,
                                     unsigned long * const out_len,
                                     unsigned long const options,
                                     const char *file,
								     long const line);
# 573 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_eval_string_ext_free (char * * pstr);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
int lr_param_increment (char * dst_name,
                              char * src_name);
# 596 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"













											  
											  

											  
											  
											  

int	  lr_save_var (char *              param_val,
							  unsigned long const param_val_len,
							  unsigned long const options,
							  char *			  param_name);
# 620 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_save_string (const char * param_val, const char * param_name);



int   lr_set_custom_error_message (const char * param_val, ...);

int   lr_remove_custom_error_message ();


int   lr_free_parameter (const char * param_name);
int   lr_save_int (const int param_val, const char * param_name);
int   lr_save_timestamp (const char * tmstampParam, ...);
int   lr_save_param_regexp (const char *bufferToScan, unsigned int bufSize, ...);

int   lr_convert_double_to_integer (const char *source_param_name, const char * target_param_name);
int   lr_convert_double_to_double (const char *source_param_name, const char *format_string, const char * target_param_name);

 
 
 
 
 
 
# 699 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_save_datetime (const char *format, int offset, const char *name);









 











 
 
 
 
 






 



char * lr_error_context_get_entry (char * key);

 



long   lr_error_context_get_error_id (void);


 
 
 

int lr_table_get_rows_num (char * param_name);

int lr_table_get_cols_num (char * param_name);

char * lr_table_get_cell_by_col_index (char * param_name, int row, int col);

char * lr_table_get_cell_by_col_name (char * param_name, int row, const char* col_name);

int lr_table_get_column_name_by_index (char * param_name, int col, 
											char * * const col_name,
											size_t * col_name_len);
# 760 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int lr_table_get_column_name_by_index_free (char * col_name);

 
 
 
 
# 775 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
int   lr_zip (const char* param1, const char* param2);
int   lr_unzip (const char* param1, const char* param2);

 
 
 
 
 
 
 
 

 
 
 
 
 
 
int   lr_param_substit (char * file,
                                   int const line,
                                   char * in_str,
                                   size_t const in_len,
                                   char * * const out_str,
                                   size_t * const out_len);
# 799 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
void   lr_param_substit_free (char * * pstr);


 
# 811 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"





char *   lrfnc_eval_string (char * str,
                                      char * file_name,
                                      long const line_num);
# 819 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


int   lrfnc_save_string ( const char * param_val,
                                     const char * param_name,
                                     const char * file_name,
                                     long const line_num);
# 825 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lrfnc_free_parameter (const char * param_name );







typedef struct _lr_timestamp_param
{
	int iDigits;
}lr_timestamp_param;

extern const lr_timestamp_param default_timestamp_param;

int   lrfnc_save_timestamp (const char * param_name, const lr_timestamp_param* time_param);

int lr_save_searched_string(char * buffer, long buf_size, unsigned int occurrence,
			    char * search_string, int offset, unsigned int param_val_len, 
			    char * param_name);

 
char *   lr_string (char * str);

 
# 926 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"

int   lr_save_value (char * param_val,
                                unsigned long const param_val_len,
                                unsigned long const options,
                                char * param_name,
                                char * file_name,
                                long const line_num);
# 933 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


 
 
 
 
 











int   lr_printf (char * fmt, ...);
 
int   lr_set_debug_message (unsigned int msg_class,
                                       unsigned int swtch);
# 955 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
unsigned int   lr_get_debug_message (void);


 
 
 
 
 

void   lr_double_think_time ( double secs);
void   lr_usleep (long);


 
 
 
 
 
 




int *   lr_localtime (long offset);


int   lr_send_port (long port);


# 1031 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"



struct _lr_declare_identifier{
	char signature[24];
	char value[128];
};

int   lr_pt_abort (void);

void vuser_declaration (void);






# 1060 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"


# 1072 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/lrun.h"
















 
 
 
 
 







int    _lr_declare_transaction   (char * transaction_name);


 
 
 
 
 







int   _lr_declare_rendezvous  (char * rendezvous_name);

 
 
 
 
 


typedef int PVCI;






typedef int VTCERR;









PVCI   vtc_connect(char * servername, int portnum, int options);
VTCERR   vtc_disconnect(PVCI pvci);
VTCERR   vtc_get_last_error(PVCI pvci);
VTCERR   vtc_query_column(PVCI pvci, char * columnName, int columnIndex, char * *outvalue);
VTCERR   vtc_query_row(PVCI pvci, int rowIndex, char * **outcolumns, char * **outvalues);
VTCERR   vtc_send_message(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_if_unique(PVCI pvci, char * column, char * message, unsigned short *outRc);
VTCERR   vtc_send_row1(PVCI pvci, char * columnNames, char * messages, char * delimiter, unsigned char sendflag, unsigned short *outUpdates);
VTCERR   vtc_update_message(PVCI pvci, char * column, int index , char * message, unsigned short *outRc);
VTCERR   vtc_update_message_ifequals(PVCI pvci, char * columnName, int index,	char * message, char * ifmessage, unsigned short 	*outRc);
VTCERR   vtc_update_row1(PVCI pvci, char * columnNames, int index , char * messages, char * delimiter, unsigned short *outUpdates);
VTCERR   vtc_retrieve_message(PVCI pvci, char * column, char * *outvalue);
VTCERR   vtc_retrieve_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues);
VTCERR   vtc_retrieve_row(PVCI pvci, char * **outcolumns, char * **outvalues);
VTCERR   vtc_rotate_message(PVCI pvci, char * column, char * *outvalue, unsigned char sendflag);
VTCERR   vtc_rotate_messages1(PVCI pvci, char * columnNames, char * delimiter, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_rotate_row(PVCI pvci, char * **outcolumns, char * **outvalues, unsigned char sendflag);
VTCERR   vtc_increment(PVCI pvci, char * column, int index , int incrValue, int *outValue);
VTCERR   vtc_clear_message(PVCI pvci, char * column, int index , unsigned short *outRc);
VTCERR   vtc_clear_column(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_ensure_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_drop_index(PVCI pvci, char * column, unsigned short *outRc);
VTCERR   vtc_clear_row(PVCI pvci, int rowIndex, unsigned short *outRc);
VTCERR   vtc_create_column(PVCI pvci, char * column,unsigned short *outRc);
VTCERR   vtc_column_size(PVCI pvci, char * column, int *size);
void   vtc_free(char * msg);
void   vtc_free_list(char * *msglist);

VTCERR   lrvtc_connect(char * servername, int portnum, int options);
VTCERR   lrvtc_disconnect();
VTCERR   lrvtc_query_column(char * columnName, int columnIndex);
VTCERR   lrvtc_query_row(int columnIndex);
VTCERR   lrvtc_send_message(char * columnName, char * message);
VTCERR   lrvtc_send_if_unique(char * columnName, char * message);
VTCERR   lrvtc_send_row1(char * columnNames, char * messages, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_update_message(char * columnName, int index , char * message);
VTCERR   lrvtc_update_message_ifequals(char * columnName, int index, char * message, char * ifmessage);
VTCERR   lrvtc_update_row1(char * columnNames, int index , char * messages, char * delimiter);
VTCERR   lrvtc_retrieve_message(char * columnName);
VTCERR   lrvtc_retrieve_messages1(char * columnNames, char * delimiter);
VTCERR   lrvtc_retrieve_row();
VTCERR   lrvtc_rotate_message(char * columnName, unsigned char sendflag);
VTCERR   lrvtc_rotate_messages1(char * columnNames, char * delimiter, unsigned char sendflag);
VTCERR   lrvtc_rotate_row(unsigned char sendflag);
VTCERR   lrvtc_increment(char * columnName, int index , int incrValue);
VTCERR   lrvtc_noop();
VTCERR   lrvtc_clear_message(char * columnName, int index);
VTCERR   lrvtc_clear_column(char * columnName); 
VTCERR   lrvtc_ensure_index(char * columnName); 
VTCERR   lrvtc_drop_index(char * columnName); 
VTCERR   lrvtc_clear_row(int rowIndex);
VTCERR   lrvtc_create_column(char * columnName);
VTCERR   lrvtc_column_size(char * columnName);



 
 
 
 
 

 
int lr_enable_ip_spoofing();
int lr_disable_ip_spoofing();


 




int lr_convert_string_encoding(char * sourceString, char * fromEncoding, char * toEncoding, char * paramName);
int lr_read_file(const char *filename, const char *outputParam, int continueOnError);


 
int lr_db_connect (char * pFirstArg, ...);
int lr_db_disconnect (char * pFirstArg,	...);
int lr_db_executeSQLStatement (char * pFirstArg, ...);
int lr_db_dataset_action(char * pFirstArg, ...);
int lr_checkpoint(char * pFirstArg,	...);
int lr_db_getvalue(char * pFirstArg, ...);







 
 



















# 1 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h" 1



 
 
 
 
# 100 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/SharedParameter.h"






typedef int PVCI2;






typedef int VTCERR2;


 
 
 

 
extern PVCI2    vtc_connect(char *servername, int portnum, int options);
extern VTCERR2  vtc_disconnect(PVCI2 pvci);
extern VTCERR2  vtc_get_last_error(PVCI2 pvci);

 
extern VTCERR2  vtc_query_column(PVCI2 pvci, char *columnName, int columnIndex, char **outvalue);
extern VTCERR2  vtc_query_row(PVCI2 pvci, int columnIndex, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_send_message(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_if_unique(PVCI2 pvci, char *column, char *message, unsigned short *outRc);
extern VTCERR2  vtc_send_row1(PVCI2 pvci, char *columnNames, char *messages, char *delimiter,  unsigned char sendflag, unsigned short *outUpdates);
extern VTCERR2  vtc_update_message(PVCI2 pvci, char *column, int index , char *message, unsigned short *outRc);
extern VTCERR2  vtc_update_message_ifequals(PVCI2 pvci, char	*columnName, int index,	char *message, char	*ifmessage,	unsigned short 	*outRc);
extern VTCERR2  vtc_update_row1(PVCI2 pvci, char *columnNames, int index , char *messages, char *delimiter, unsigned short *outUpdates);
extern VTCERR2  vtc_retrieve_message(PVCI2 pvci, char *column, char **outvalue);
extern VTCERR2  vtc_retrieve_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues);
extern VTCERR2  vtc_retrieve_row(PVCI2 pvci, char ***outcolumns, char ***outvalues);
extern VTCERR2  vtc_rotate_message(PVCI2 pvci, char *column, char **outvalue, unsigned char sendflag);
extern VTCERR2  vtc_rotate_messages1(PVCI2 pvci, char *columnNames, char *delimiter, char ***outvalues, unsigned char sendflag);
extern VTCERR2  vtc_rotate_row(PVCI2 pvci, char ***outcolumns, char ***outvalues, unsigned char sendflag);
extern VTCERR2	vtc_increment(PVCI2 pvci, char *column, int index , int incrValue, int *outValue);
extern VTCERR2  vtc_clear_message(PVCI2 pvci, char *column, int index , unsigned short *outRc);
extern VTCERR2  vtc_clear_column(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_clear_row(PVCI2 pvci, int rowIndex, unsigned short *outRc);

extern VTCERR2  vtc_create_column(PVCI2 pvci, char *column,unsigned short *outRc);
extern VTCERR2  vtc_column_size(PVCI2 pvci, char *column, int *size);
extern VTCERR2  vtc_ensure_index(PVCI2 pvci, char *column, unsigned short *outRc);
extern VTCERR2  vtc_drop_index(PVCI2 pvci, char *column, unsigned short *outRc);

extern VTCERR2  vtc_noop(PVCI2 pvci);

 
extern void vtc_free(char *msg);
extern void vtc_free_list(char **msglist);

 


 




 




















 




 
 
 

extern VTCERR2  lrvtc_connect(char *servername, int portnum, int options);
extern VTCERR2  lrvtc_disconnect();
extern VTCERR2  lrvtc_query_column(char *columnName, int columnIndex);
extern VTCERR2  lrvtc_query_row(int columnIndex);
extern VTCERR2  lrvtc_send_message(char *columnName, char *message);
extern VTCERR2  lrvtc_send_if_unique(char *columnName, char *message);
extern VTCERR2  lrvtc_send_row1(char *columnNames, char *messages, char *delimiter,  unsigned char sendflag);
extern VTCERR2  lrvtc_update_message(char *columnName, int index , char *message);
extern VTCERR2  lrvtc_update_message_ifequals(char *columnName, int index, char 	*message, char *ifmessage);
extern VTCERR2  lrvtc_update_row1(char *columnNames, int index , char *messages, char *delimiter);
extern VTCERR2  lrvtc_retrieve_message(char *columnName);
extern VTCERR2  lrvtc_retrieve_messages1(char *columnNames, char *delimiter);
extern VTCERR2  lrvtc_retrieve_row();
extern VTCERR2  lrvtc_rotate_message(char *columnName, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_messages1(char *columnNames, char *delimiter, unsigned char sendflag);
extern VTCERR2  lrvtc_rotate_row(unsigned char sendflag);
extern VTCERR2  lrvtc_increment(char *columnName, int index , int incrValue);
extern VTCERR2  lrvtc_clear_message(char *columnName, int index);
extern VTCERR2  lrvtc_clear_column(char *columnName);
extern VTCERR2  lrvtc_clear_row(int rowIndex);
extern VTCERR2  lrvtc_create_column(char *columnName);
extern VTCERR2  lrvtc_column_size(char *columnName);
extern VTCERR2  lrvtc_ensure_index(char *columnName);
extern VTCERR2  lrvtc_drop_index(char *columnName);

extern VTCERR2  lrvtc_noop();

 
 
 

                               


 
 
 





















# 2 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

# 1 "globals.h" 1



 
 

# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 1







# 1 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h" 1



























































 




 



 











 





















 
 
 

  int
	web_add_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_add_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
	
  int
	web_add_auto_header(
		const char *		mpszHeader,
		const char *		mpszValue);

  int
	web_add_header(
		const char *		mpszHeader,
		const char *		mpszValue);
  int
	web_add_cookie(
		const char *		mpszCookie);
  int
	web_cleanup_auto_headers(void);
  int
	web_cleanup_cookies(void);
  int
	web_concurrent_end(
		const char * const	mpszReserved,
										 
		...								 
	);
  int
	web_concurrent_start(
		const char * const	mpszConcurrentGroupName,
										 
										 
		...								 
										 
	);
  int
	web_create_html_param(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim);
  int
	web_create_html_param_ex(
		const char *		mpszParamName,
		const char *		mpszLeftDelim,
		const char *		mpszRightDelim,
		const char *		mpszNum);
  int
	web_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_custom_request(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_disable_keep_alive(void);
  int
	web_enable_keep_alive(void);
  int
	web_find(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_get_int_property(
		const int			miHttpInfoType);
  int
	web_image(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_image_check(
		const char *		mpszName,
		...);
  int
	web_java_check(
		const char *		mpszName,
		...);
  int
	web_link(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

	
  int
	web_global_verification(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
  int
	web_reg_find(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
				
  int
	web_reg_save_param(
		const char *		mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 

  int
	web_convert_param(
		const char * 		mpszParamName, 
										 
		...);							 
										 
										 


										 

										 
  int
	web_remove_auto_filter(
		const char *		mpszArg,
		...
	);									 
										 
				
  int
	web_remove_auto_header(
		const char *		mpszHeaderName,
		...);							 
										 



  int
	web_remove_cookie(
		const char *		mpszCookie);

  int
	web_save_header(
		const char *		mpszType,	 
		const char *		mpszName);	 
  int
	web_set_certificate(
		const char *		mpszIndex);
  int
	web_set_certificate_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_set_connections_limit(
		const char *		mpszLimit);
  int
	web_set_max_html_param_len(
		const char *		mpszLen);
  int
	web_set_max_retries(
		const char *		mpszMaxRetries);
  int
	web_set_proxy(
		const char *		mpszProxyHost);
  int
	web_set_pac(
		const char *		mpszPacUrl);
  int
	web_set_proxy_bypass(
		const char *		mpszBypass);
  int
	web_set_secure_proxy(
		const char *		mpszProxyHost);
  int
	web_set_sockets_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue
	);
  int
	web_set_option(
		const char *		mpszOptionID,
		const char *		mpszOptionValue,
		...								 
	);
  int
	web_set_timeout(
		const char *		mpszWhat,
		const char *		mpszTimeout);
  int
	web_set_user(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);

  int
	web_sjis_to_euc_param(
		const char *		mpszParamName,
										 
		const char *		mpszParamValSjis);
										 

  int
	web_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	spdy_submit_data(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_submit_form(
		const char *		mpszStepName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										  
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
  int
	web_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	spdy_url(
		const char *		mpszUrlName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_set_proxy_bypass_local(
		const char * mpszNoLocal
		);

  int 
	web_cache_cleanup(void);

  int
	web_create_html_query(
		const char* mpszStartQuery,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int 
	web_create_radio_button_param(
		const char *NameFiled,
		const char *NameAndVal,
		const char *ParamName
		);

  int
	web_convert_from_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 
										
  int
	web_convert_to_formatted(
		const char * mpszArg1,
		...);							 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_ex(
		const char * mpszParamName,
		...);							 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_xpath(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_json(
		const char * mpszParamName,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_reg_save_param_regexp(
		 const char * mpszParamName,
		 ...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_run(
		const char * mpszCode,
		...);							
										 
										 
										 
										 
										 
										 
										 
										 
										 

  int
	web_js_reset(void);

  int
	web_convert_date_param(
		const char * 		mpszParamName,
		...);










# 769 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"


# 782 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"



























# 820 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"

 
 
 


  int
	FormSubmit(
		const char *		mpszFormName,
		...);
  int
	InitWebVuser(void);
  int
	SetUser(
		const char *		mpszUserName,
		const char *		mpszPwd,
		const char *		mpszHost);
  int
	TerminateWebVuser(void);
  int
	URL(
		const char *		mpszUrlName);
























# 888 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/as_web.h"


  int
	web_rest(
		const char *		mpszReqestName,
		...);							 
										 
										 
										 
										 

  int
web_stream_open(
	const char *		mpszArg1,
	...
);
  int
	web_stream_wait(
		const char *		mpszArg1,
		...
	);

  int
	web_stream_close(
		const char *		mpszArg1,
		...
	);

  int
web_stream_play(
	const char *		mpszArg1,
	...
	);

  int
web_stream_pause(
	const char *		mpszArg1,
	...
	);

  int
web_stream_seek(
	const char *		mpszArg1,
	...
	);

  int
web_stream_get_param_int(
	const char*			mpszStreamID,
	const int			miStateType
	);

  double
web_stream_get_param_double(
	const char*			mpszStreamID,
	const int			miStateType
	);

  int
web_stream_get_param_string(
	const char*			mpszStreamID,
	const int			miStateType,
	const char*			mpszParameterName
	);

  int
web_stream_set_param_int(
	const char*			mpszStreamID,
	const int			miStateType,
	const int			miStateValue
	);

  int
web_stream_set_param_double(
	const char*			mpszStreamID,
	const int			miStateType,
	const double		mdfStateValue
	);

 
 
 






# 9 "C:\\Program Files (x86)\\HP\\LoadRunner\\include/web_api.h" 2

















 







 














  int
	web_reg_add_cookie(
		const char *		mpszCookie,
		...);							 
										 

  int
	web_report_data_point(
		const char *		mpszEventType,
		const char *		mpszEventName,
		const char *		mpszDataPointName,
		const char *		mpszLAST);	 
										 
										 
										 

  int
	web_text_link(
		const char *		mpszStepName,
		...);

  int
	web_element(
		const char *		mpszStepName,
		...);

  int
	web_image_link(
		const char *		mpszStepName,
		...);

  int
	web_static_image(
		const char *		mpszStepName,
		...);

  int
	web_image_submit(
		const char *		mpszStepName,
		...);

  int
	web_button(
		const char *		mpszStepName,
		...);

  int
	web_edit_field(
		const char *		mpszStepName,
		...);

  int
	web_radio_group(
		const char *		mpszStepName,
		...);

  int
	web_check_box(
		const char *		mpszStepName,
		...);

  int
	web_list(
		const char *		mpszStepName,
		...);

  int
	web_text_area(
		const char *		mpszStepName,
		...);

  int
	web_map_area(
		const char *		mpszStepName,
		...);

  int
	web_eval_java_script(
		const char *		mpszStepName,
		...);

  int
	web_reg_dialog(
		const char *		mpszArg1,
		...);

  int
	web_reg_cross_step_download(
		const char *		mpszArg1,
		...);

  int
	web_browser(
		const char *		mpszStepName,
		...);

  int
	web_control(
		const char *		mpszStepName,
		...);

  int
	web_set_rts_key(
		const char *		mpszArg1,
		...);

  int
	web_save_param_length(
		const char * 		mpszParamName,
		...);

  int
	web_save_timestamp_param(
		const char * 		mpszParamName,
		...);

  int
	web_load_cache(
		const char *		mpszStepName,
		...);							 
										 

  int
	web_dump_cache(
		const char *		mpszStepName,
		...);							 
										 
										 

  int
	web_reg_find_in_log(
		const char *		mpszArg1,
		...);							 
										 
										 

  int
	web_get_sockets_info(
		const char *		mpszArg1,
		...);							 
										 
										 
										 
										 

  int
	web_add_cookie_ex(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

  int
	web_hook_java_script(
		const char *		mpszArg1,
		...);							 
										 
										 
										 

 
 
 
 
 
 
 
 
 
 
 
 
  int
	web_reg_async_attributes(
		const char *		mpszArg,
		...
	);

 
 
 
 
 
 
  int
	web_sync(
		 const char *		mpszArg1,
		 ...
	);

 
 
 
 
  int
	web_stop_async(
		const char *		mpszArg1,
		...
	);

 
 
 
 
 

 
 
 

typedef enum WEB_ASYNC_CB_RC_ENUM_T
{
	WEB_ASYNC_CB_RC_OK,				 

	WEB_ASYNC_CB_RC_ABORT_ASYNC_NOT_ERROR,
	WEB_ASYNC_CB_RC_ABORT_ASYNC_ERROR,
										 
										 
										 
										 
	WEB_ASYNC_CB_RC_ENUM_COUNT
} WEB_ASYNC_CB_RC_ENUM;

 
 
 

typedef enum WEB_CONVERS_CB_CALL_REASON_ENUM_T
{
	WEB_CONVERS_CB_CALL_REASON_BUFFER_RECEIVED,
	WEB_CONVERS_CB_CALL_REASON_END_OF_TASK,

	WEB_CONVERS_CB_CALL_REASON_ENUM_COUNT
} WEB_CONVERS_CB_CALL_REASON_ENUM;

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

typedef
int														 
	(*RequestCB_t)();

typedef
int														 
	(*ResponseBodyBufferCB_t)(
		  const char *		aLastBufferStr,
		  int				aLastBufferLen,
		  const char *		aAccumulatedStr,
		  int				aAccumulatedLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseCB_t)(
		  const char *		aResponseHeadersStr,
		  int				aResponseHeadersLen,
		  const char *		aResponseBodyStr,
		  int				aResponseBodyLen,
		  int				aHttpStatusCode);

typedef
int														 
	(*ResponseHeadersCB_t)(
		  int				aHttpStatusCode,
		  const char *		aAccumulatedHeadersStr,
		  int				aAccumulatedHeadersLen);



 
 
 

typedef enum WEB_CONVERS_UTIL_RC_ENUM_T
{
	WEB_CONVERS_UTIL_RC_OK,
	WEB_CONVERS_UTIL_RC_CONVERS_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_TASK_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_NOT_FOUND,
	WEB_CONVERS_UTIL_RC_INFO_UNAVIALABLE,
	WEB_CONVERS_UTIL_RC_INVALID_ARGUMENT,

	WEB_CONVERS_UTIL_RC_ENUM_COUNT
} WEB_CONVERS_UTIL_RC_ENUM;

 
 
 

  int					 
	web_util_set_request_url(
		  const char *		aUrlStr);

  int					 
	web_util_set_request_body(
		  const char *		aRequestBodyStr);

  int					 
	web_util_set_formatted_request_body(
		  const char *		aRequestBodyStr);


 
 
 
 
 

 
 
 
 
 

 
 
 
 
 
 
 
 

 
 
  int
web_websocket_connect(
		 const char *	mpszArg1,
		 ...
		 );


 
 
 
 
 																						
  int
web_websocket_send(
	   const char *		mpszArg1,
		...
	   );

 
 
 
 
 
 
  int
web_websocket_close(
		const char *	mpszArg1,
		...
		);

 
typedef
void														
(*OnOpen_t)(
			  const char* connectionID,  
			  const char * responseHeader,  
			  int length  
);

typedef
void														
(*OnMessage_t)(
	  const char* connectionID,  
	  int isbinary,  
	  const char * data,  
	  int length  
	);

typedef
void														
(*OnError_t)(
	  const char* connectionID,  
	  const char * message,  
	  int length  
	);

typedef
void														
(*OnClose_t)(
	  const char* connectionID,  
	  int isClosedByClient,  
	  int code,  
	  const char* reason,  
	  int length  
	 );
 
 
 
 
 





# 7 "globals.h" 2

# 1 "lrw_custom_body.h" 1
 




# 8 "globals.h" 2


 
 


# 3 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

# 1 "vuser_init.c" 1
vuser_init()
{
	return 0;
}
# 4 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

# 1 "Action.c" 1
Action()
{
	web_set_max_html_param_len("1024");
	web_set_sockets_option("SSL_VERSION", "TLS1.2");
web_reg_save_param("authToken", "LB=\"authToken\":\"", "RB=\",\"user","ORD=ALL","LAST");
	lr_start_transaction("STRA_UAT_01_Login");
	 
	web_rest("POST: {UATURL}/common/v1/User",
		"URL={UATURL}/common/v1/User",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t940759.inf",
		"Body={\n"
		"\"userId\": \"{USERNAME}\",\n"	
		"\"password\": \"{PASSWORD}\",\n"
		"\"channel\": \"string\",\n"
		"\"lang\": \"string\",\n"
		"\"identification\": {\n"
		"\"deviceId\": \"string\"\n"
		"},\n"
		"\"terms\": \"string\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=DeviceInfo", "Value=DeviceID:PrakashTesting1, AppName: Sentara, AppVersion: 1.0, IP Address:1.1.1.1, MAC Address:", "ENDHEADER",
		"LAST");
	
	if(atoi(lr_eval_string("{authToken_count}"))>0)
    {
 lr_end_transaction("STRA_UAT_01_Login", 0);
 lr_output_message("LoginPassed  %s",lr_eval_string("{USERNAME}"));
}
 else
 {
 lr_end_transaction("STRA_SP2_01_Login", 1);
 lr_error_message("Login failed,  %s received", lr_eval_string("{USERNAME}"));
  
 lr_exit(5, 1);
 }
	 
	lr_start_transaction("STRA_UAT_Authenticated_02_Allergies");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Allergies",
		"Method=GET",
		"Snapshot=t143629.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	lr_end_transaction("STRA_UAT_Authenticated_02_Allergies", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_UAT_Authenticated_03_CurrentHealthIssues");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/CurrentHealthIssues",
		"Method=GET",
		"Snapshot=t51781.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_UAT_Authenticated_03_CurrentHealthIssues", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_UAT_Authenticated_04_Immunizations");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Immunizations",
		"Method=GET",
		"Snapshot=t323762.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_UAT_Authenticated_04_Immunizations", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_UAT_Authenticated_05_PatientMedications");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Medications",
		"Method=GET",
		"Snapshot=t958799.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_UAT_Authenticated_05_PatientMedications", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_UAT_Authenticated_06_MedicalRecord");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/MedicalRecord",
		"Method=GET",
		"Snapshot=t931622.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	lr_end_transaction("STRA_UAT_Authenticated_06_MedicalRecord", 2);
	 
	lr_think_time(5);

 
	lr_start_transaction("STRA_UAT_Authenticated_07_HealthSummary");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/HealthSummary?platform=mobile",
		"Method=GET",
		"Snapshot=t361116.inf",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	
	lr_end_transaction("STRA_UAT_Authenticated_07_HealthSummary", 2);
	lr_think_time(5);
	 

 
	lr_start_transaction("STRA_UAT_Authenticated_08_PreventiveCare");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PreventiveCare",
		"Method=GET",
		"Snapshot=t435586.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_Authenticated_08_PreventiveCare", 2);
 
	lr_think_time(5);
 
	lr_start_transaction("STRA_UAT_Authenticated_09_PCP");
	 
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/PCP",
		 
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/PCP",
		"Method=GET",
		"Snapshot=t505984.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_UAT_Authenticated_09_PCP", 2);
	 
	lr_think_time(5);
	
 
 
 
 
 
 
 
 
 
	 
	lr_start_transaction("STRA_UAT_Authenticated_10_Providers");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t46687.inf",
		"Body={\n"
		"\"time\": {\n"
		"\"from\": \"\",\n"
		"\"to\": \"\"\n"
		"},\n"
		"\"reasonId\": \"\",\n"
		"\"date\": \"\",\n"
		"\"providerName\": \"\",\n"
		"\"providerType\": \"\",\n"
		"\"gender\": \"\",\n"
		"\"zipcode\": \"{ZIPCODE}\",\n"
		"\"latitude\": \"\",\n"
		"\"longitude\": \"\",\n"
		"\"radius\": 10,\n"
		"\"languages\": \"\",\n"
		"\"specialty\": \"\",\n"
		"\"offset\":\"1\",\n"
		"\"limit\":\"40\",\n"
		"\"showPrimaryCareProviders\": \"true\",\n"
		"\"onlyShowProvidersWhoScheduleOnline\": \"true\",\n"
		"\"showNonSentaraProviders\":\"true\",\n"
		"\"smg\":0\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	
	lr_end_transaction("STRA_UAT_Authenticated_10_Providers", 2);
	 
	 
	 
	lr_think_time(5);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	lr_start_transaction("STRA_UAT_Authenticated_11_ProviderDetails");
	 
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/7832?idType=epi",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/7832?idType=epi",
		"Method=GET",
		"Snapshot=t437254.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_Authenticated_11_ProviderDetails", 2);
	 
	lr_output_message("ProviderID value is %s",lr_eval_string("{Provider_ID}"));
	lr_output_message("DepartmentIDRes is %s",lr_eval_string("{Department_ID}"));
	lr_output_message("NPI Value is %s",lr_eval_string("{NPI_ID}")); 
	lr_think_time(5);
	
	
	 
 
# 271 "Action.c"
 
	lr_start_transaction("STRA_UAT_Authenticated_12_OpenSlots");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/OpenSlots",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t881379.inf",
		"Body={\n"
		 
		"\"providerId\": \"4221\",\n"
		"\"providerType\": \"External\",\n"
		"\"visitTypeId\": \"1099\",\n"
		"\"visitType\": \"External\",\n"
		 
		"\"departmentId\": \"20837003\",\n"
		"\"departmentType\": \"External\",\n"
		"\"startDate\": \"{START_DATE}\",\n"
		"\"endDate\": \"{END_DATE}\",\n"
		"\"showNextAvailableSlots\": true\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	lr_end_transaction("STRA_UAT_Authenticated_12_OpenSlots", 2);
	 
 





	 
 




 
	lr_start_transaction("STRA_UAT_Authenticated_13_ActiveAppoinments");
	web_reg_save_param("ACTIVEApponmentsRes", "LB=", "RB=","LAST");
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Appointments?Type=1&offset=1&limit=20",
		"Method=GET",
		"Snapshot=t4250.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_Authenticated_13_ActiveAppoinments", 2);
	 
	 
	lr_think_time(5);

 
	 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
# 384 "Action.c"

 

	lr_start_transaction("STRA_SP3_15_ProviderVisitType");
	 
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		 
		 
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"Method=GET",
		"Snapshot=t381117.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP3_15_ProviderVisitType", 2);
	 
	lr_think_time(5);

 
	lr_start_transaction("STRA_SP3_16_VisitTypeEstablished");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/Established/Reasons",
		"Method=GET",
		"Snapshot=t124016.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP3_16_VisitTypeEstablished", 2);
	 
	lr_think_time(5);
	
 
	lr_start_transaction("STRA_SP3_17_VisitTypeNew");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"Method=GET",
		"Snapshot=t157300.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP3_17_VisitTypeNew", 2);
	 
	lr_think_time(5);

 
	lr_start_transaction("STRA_SP3_18_VisitTypeVirtualCare");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/VirtualCare/Reasons",
		"Method=GET",
		"Snapshot=t732626.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP3_18_VisitTypeVirtualCare", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_SP3_19_VisitTypeSameday");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/SameDay/Reasons",
		"Method=GET",
		"Snapshot=t247965.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP3_19_VisitTypeSameday", 2);
	 
	lr_think_time(5);
	
	 

 
 
 
 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


	 
	web_reg_save_param("LocationID",
	                   "LB=\"locationTypeId\":",
	                   "RB=,\"",
	                   "ORD=8",
	                   "LAST");
 
	lr_start_transaction("STRA_SP3_27_LocationType");
	 
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Facilities/LocationTypes",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities/LocationTypes",
		"Method=GET",
		"Snapshot=t569225.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_27_LocationType", 2);
	 
	lr_output_message("LocationId value is %s",lr_eval_string("{LocationID}"));
	lr_think_time(5);

	 
	web_reg_save_param("FacilityID",
	                   "LB=\"facilityId\":",
	                   "RB=,\"",
	                    "LAST");
	                    
 
	lr_start_transaction("STRA_SP3_28_FindFacility");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities?zipCode=23462&locationType={LocationID}&distance=50",
		"Method=GET",
		"Snapshot=t548567.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_28_FindFacility", 2);
	 
	lr_output_message("Findfacility ID value is %s",lr_eval_string("{FacilityID}"));
	lr_think_time(5);
 
	lr_start_transaction("STRA_SP3_29_FacilityDetails");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities/{FacilityID}",
		"Method=GET",
		"Snapshot=t718694.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_29_FacilityDetails", 2);
	 
	lr_think_time(5);
 
	lr_start_transaction("STRA_SP3_30_ReviewComment");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/1528033180/ReviewComments?limit=20&offset=1",
		"Method=GET",
		"Snapshot=t42572.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_30_ReviewComment", 2);
	 
	lr_think_time(5);
 
 
	lr_start_transaction("STRA_SP3_31_ProviderImage");
	 
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/8839/Image",
		 
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/8839/Image",
		"Method=GET",
		"Snapshot=t197389.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_31_ProviderImage", 2);
	 
		
 

	lr_start_transaction("STRA_SP4_02_OrderStatus");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/orders/status",
		"Method=GET",
		"Snapshot=t28307.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_02_OrderStatus", 2);
	 

	lr_think_time(3);
	 
	web_reg_save_param("flowsheetId", "LB=\"flowsheetId\":\"", "RB=\"","LAST");
	 
	web_reg_save_param("flowsheetIdType", "LB=\"flowsheetIdType\":\"", "RB=\"","LAST");
	 
	web_reg_save_param("StartDate", "LB=\"startDate\":\"", "RB=\"","LAST");
 
	lr_start_transaction("STRA_SP4_03_HealthTracks");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"Method=GET",
		"Snapshot=t527050.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP4_03_HealthTracks", 2);
	 
	lr_think_time(3);


 
	lr_start_transaction("STRA_SP4_04_HealthTracksDetails");
	 
	web_rest("POST: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t588550.inf",
		"Body={\n"
		"\"flowsheetId\": \"{flowsheetId}\",\n"
		"\"flowsheetIdType\": \"{flowsheetIdType}\",\n"
		"\"startDate\": \"{StartDate}\",\n"
		"\"endDate\": \"\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_04_HealthTracksDetails", 2);
	 
	lr_think_time(3);
	 
	web_reg_save_param("Logid",
	                   "LB=\"logId\":\"",
	                   "RB=\"",
	                   "LAST");
 
	lr_start_transaction("STRA_SP4_05_LogTypes");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes",
		"Method=GET",
		"Snapshot=t867331.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_05_LogTypes", 2);
 
 
	lr_think_time(3);
 
# 777 "Action.c"
	 
	web_reg_save_param("SMID",
	                   "LB=\"messageId\":\"",
	                   "RB=\"",
	                   "LAST");
 
	lr_start_transaction("STRA_SP4_08_SendMessage");
	 
	web_rest("GET: {UATURL}/consumer/v1/patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/sentMessages?limit=20",
		"Method=GET",
		"Snapshot=t473769.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_08_SendMessage", 2);
	lr_think_time(3);
 
 
 
	lr_start_transaction("STRA_SP4_09_SendMessageDetail");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/sentMessages/{SMID}",
		"Method=GET",
		"Snapshot=t904766.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_09_SendMessageDetail", 2);
	 
 
	lr_start_transaction("STRA_SP4_10_ContactDoctor");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/patient/messages/contactDoctor",
		"Method=GET",
		"Snapshot=t95798.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_10_ContactDoctor", 2);
	lr_think_time(3);
	 

 
	lr_start_transaction("STRA_SP4_11_HealthTrackGraph");
	 
	web_rest("POST: {UATURL}/consumer/v1/Patient/HealthRecords/healthTracks/graph",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/healthTracks/graph",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t639981.inf",
		"Body={\n"
		"\"flowsheetId\": \"{flowsheetId}\",\n"
		"\"flowsheetIdType\": \"INTERNAL\",\n"
		"\"startDate\": \"2018-07-12\",\n"
		 
		"\"componentIds\": null\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_11_HealthTrackGraph", 2);
	lr_think_time(3);
	 
	
	 
	web_reg_save_param("Mid",
	                   "LB=\"False\",\"id\":\"",
	                   "RB=\"",
	                   "LAST");
 
	lr_start_transaction("STRA_SP4_12_Message");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages",
		"Method=GET",
		"Snapshot=t685063.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_12_Message", 2);
 
 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_13_MessageDetails");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/Messages/{Mid}",
		"URL={UATURL}/consumer/v1/Patient/Messages/{Mid}",
		"Method=GET",
		"Snapshot=t255618.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_13_MessageDetails", 2);
	 
	lr_think_time(3);
	

 
# 914 "Action.c"



 
	lr_start_transaction("STRA_SP4_15_WalletCard");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/walletCard",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/walletCard",
		"Method=GET",
		"Snapshot=t396856.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_15_WalletCard", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_16_OrderExpired");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/orders/2/1/5",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/orders/2/1/5",
		"Method=GET",
		"Snapshot=t934448.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_16_OrderExpired", 2);
	 
	lr_think_time(3);

 
	lr_start_transaction("STRA_SP4_17_ContactCustomerService");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/customerService",
		"Method=GET",
		"Snapshot=t55893.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_17_ContactCustomerService", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_18_NewMessage");
	 
	web_rest("POST: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t909718.inf",
		"ITEMDATA",
		"Name=To", "Value=P10443", "ENDITEM",
		"Name=Subject", "Value=Contact Customer Service6:26", "ENDITEM",
		"Name=MessageType", "Value=14", "ENDITEM",
		"Name=Body", "Value=Test Body", "ENDITEM",
		"Name=Attachments", "FileName=", "ENDITEM",
		"HEADERS",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_18_NewMessage", 2);
	 
	lr_think_time(3);
 
	 
	lr_start_transaction("STRA_SP4_19_OpenOrder");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/orders/1/1/5",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/orders/1/1/5",
		"Method=GET",
		"Snapshot=t201472.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_19_OpenOrder", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_20_WalletCardinfo");
	 
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/WalletCard/ContactInfo",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/WalletCard/ContactInfo",
		 
		"Method=GET",
		"Snapshot=t598266.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_20_WalletCardinfo", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_21_TestType");
	 
	web_rest("GET: {UATURL}/consumer/v1/patient/HealthRecords/HealthTracks/tests/{flowsheetId}/{flowsheetIdType}",
		"URL={UATURL}/consumer/v1/patient/HealthRecords/HealthTracks/tests/{flowsheetId}/{flowsheetIdType}",
		"Method=GET",
		"Snapshot=t50491.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_21_TestType", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_22_UpdateEmail");
	 
	web_rest("PUT: {UATURL}/consumer/v1/Patient/Profile/Demographics",
		"URL={UATURL}/consumer/v1/Patient/Profile/Demographics",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t370909.inf",
		"Body={\n"
		"\"demographics\": {\n"
		"\"homeaddress\": {\n"
		"\"houseNumber\": \"\",\n"
		"\"streetAddress\": [\n"
		"\"800 INDEPENDENCE BLVD\"\n"
		"],\n"
		"\"city\": \"VIRGINIA BEACH\",\n"
		"\"state\": \"VA\",\n"
		"\"zipCode\": \"23455-6011\",\n"
		"\"county\": \"Virginia Beach City\",\n"
		"\"country\": \"US\",\n"
		"\"district\": \"\"\n"
		"},\n"
		"\"language\": \"English\",\n"
		"\"emails\": [\n"
		"\"kollaradhakrish@photoninfotech.net\"\n"
		"],\n"
		"\"phoneNumbers\": [\n"
		"{\n"
		"\"phone\": \"757-222-4545\",\n"
		"\"type\": \"mobile\"\n"
		"},\n"
		"{\n"
		"\"phone\": \"000-000-0000\",\n"
		"\"type\": \"Home Phone\"\n"
		"}\n"
		"]\n"
		"}\n"
		"}  \n",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_22_UpdateEmail", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_23_UpdateQuestions");
	 
	web_rest("PUT: {UATURL}/consumer/v1/Qu...",
		"URL={UATURL}/consumer/v1/Questions",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t881888.inf",
		"Body={"
		"\"questions\": ["
		"{"
		"\"question\": \"What elementary school did you attend?\","
		"\"answer\": \"GMI\""
		"},"
		"{"
		"\"question\": \"What was the model of your first car?\","
		"\"answer\": \"Renault\""
		"},"
		"{"
		"\"question\": \"Who was your childhood hero?\","
		"\"answer\": \"Popoye\""
		"}"
		"]"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_23_UpdateQuestions", 2);
	 
	lr_think_time(3);

 
	lr_start_transaction("STRA_SP4_24_FPHTriEmail");
	 
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/password",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t701670.inf",
		"Body={"
		"\"user\": {"
		"\"userId\": \"{USERNAME}\""
		"},"
		"\"channel\": \"Web\","
		"\"lang\": \"en\""
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_24_FPHTriEmail", 2);
	 
	lr_think_time(3);
	 
	 
	
	
 
	lr_start_transaction("STRA_SP4_25_FogUNameVerifyUser");
	 
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/verify",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t176171.inf",
		"Body={"
		"\"ssn\": \"5317\","
		"\"dob\": \"1954-12-20\","
		"\"email\": \"mtkrishn@sentara.com\","
		"\"channel\": \"Web\","
		"\"lang\": \"en\""
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_25_FogUNameVerifyUser", 2);
	 
	 
	lr_think_time(3);
	
 
	lr_start_transaction("STRA_SP4_26_FogUNStepUp");
	 
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/stepup",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t434210.inf",
		"Body={\n"
		"\"token\": \"{authToken_1}\",\n"
		"\"secretQuestion\": \"Who was your childhood hero?\",\n"
		"\"secretAnswer\": \"CaptainAmerica\",\n"
		"\"channel\": \"Web\",\n"
		"\"lang\": \"en\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_26_FogUNStepUp", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_27_GetAllQuestions");
	 
	web_rest("GET: {UATURL}/common/v1/Questions",
		"URL={UATURL}/common/v1/Questions",
		"Method=GET",
		"Snapshot=t427331.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_27_GetAllQuestions", 2);
	 
	lr_think_time(3);

 
	lr_start_transaction("STRA_SP4_29_GetProfile");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile",
		"Method=GET",
		"Snapshot=t461914.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_29_GetProfile", 2);
	 
	lr_think_time(3);


	 
	 
 
	lr_start_transaction("STRA_SP4_34_RequestForSSOToken");
	 
	web_rest("GET: {UATURL}/consumer/v1/Ch...",
		"URL={UATURL}/consumer/v1/Chart/sso",
		"Method=GET",
		"Snapshot=t541683.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_34_RequestForSSOToken", 2);
	 
	 
	lr_think_time(3);

 
	lr_start_transaction("STRA_SP4_36_CheckPatientexisttrueW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PatientExistence",
		"Method=GET",
		"Snapshot=t198733.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_36_CheckPatientexisttrueW", 2);
	 
	lr_think_time(3);

	 
	web_reg_save_param("VisitTypeId",
	                   "LB=\"id\":",
	                   "RB=,\"",
	                   "LAST");
 
	lr_start_transaction("STRA_SP4_39_ImageVisitTypeW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/visittypes",
		"Method=GET",
		"Snapshot=t852857.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_39_ImageVisitTypeW", 2);
 
 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_40_ImagingQuestionnaireW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/visittypes/{VisitTypeId}/questionnaire",
		"Method=GET",
		"Snapshot=t658115.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_40_ImagingQuestionnaireW", 2);
	 
	lr_think_time(3);
	
 
	lr_start_transaction("STRA_SP4_41_CountryW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/Country",
		"Method=GET",
		"Snapshot=t265796.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_41_CountryW", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_42_CountyW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/County",
		"Method=GET",
		"Snapshot=t892501.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_42_CountyW", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_43_StatesW");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/States",
		"Method=GET",
		"Snapshot=t368305.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_43_StatesW", 2);
	 
	lr_think_time(3);
 
# 1391 "Action.c"
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	lr_think_time(3);
 
 
# 1486 "Action.c"
 
# 1629 "Action.c"







 	                   
# 1659 "Action.c"
	
 
	 
	 
 
 
 
 
 
 
 
 
 

	 
	 
	 


 
	lr_start_transaction("STRA_SP4_54_2DSchedule");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/2DMammogram/Schedule",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t927331.inf",
		"Body={ \n"
		"\"reasonId\": \"1103\", \n"
		"\"appointmentId\": \"\", \n"
		"\"date\": \"{OPENDATE}\",\n"
		"\"time\": \"{TimeStamp}\",\n"
		 
		"\"NPI\": \"\", \n"
		"\"waitlist\": false, \n"
		"\"phone\": \"\", \n"
		"\"concern\": \"\", \n"
		"\"concernDetails\": \"\", \n"
		"\"commDetails\": \"\", \n"
		"\"visitType\": \"EXTERNAL\", \n"
		 
		"\"departmentId\": \"20837003\", \n"
		"\"departmentType\": \"EXTERNAL\", \n"
		"\"providerType\": \"NPI\", \n"
		"\"duration\": \"{DurationTime}\"\n"
		"}\n",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_54_2DSchedule", 2);
	 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_55_3DSchedule");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/3DMammogram/Schedule",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t701473.inf",
		"Body={ \n"
		"\"reasonId\": \"1103\", \n"
		"\"appointmentId\": \"\", \n"
		"\"date\": \"{OPENDATE}\",\n"
		"\"time\": \"{TimeStamp1}\",\n"
		 
		"\"NPI\": \"\", \n"
		"\"waitlist\": false, \n"
		"\"phone\": \"\", \n"
		"\"concern\": \"\", \n"
		"\"concernDetails\": \"\", \n"
		"\"commDetails\": \"\", \n"
		"\"visitType\": \"EXTERNAL\", \n"
		 
		"\"departmentId\": \"20837003\", \n"
		"\"departmentType\": \"EXTERNAL\", \n"
		"\"providerType\": \"NPI\", \n"
		"\"duration\": \"{DurationTime}\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_55_3DSchedule", 2);
	 
	lr_think_time(3);

	 
 
 
 
 
 
 
 
 
 
 
	lr_start_transaction("STRA_SP4_56_GetSessionDevicess");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/sessiondevices",
		"Method=GET",
		"Snapshot=t763765.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_56_GetSessionDevicess", 2);
 
 
 
	lr_think_time(3);
 
	lr_start_transaction("STRA_SP4_57_PostSessionDevicess");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/sessiondevices",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t931578.inf",
		"Body={\n"
		"\"sessionId\": \"{authToken_1}\",\n"
		"\"deviceDescription\": \"{DEVICE}\",\n"
		"\"clientDescription\": \"{CLINTDES}\",\n"
		"\"location\": {\n"
		"\"zipcode\": \"23462\",\n"
		"\"latitude\": \"36.8400852\",\n"
		"\"longitude\": \"-76.1493891\"\n"
		"}\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_57_PostSessionDevicess", 2);
	 

	 
	web_reg_save_param("TID",
	                   "LB=\"testId\":\"",
	                   "RB=\"",
	                   "convert=HTML_TO_URL",
	                    "LAST");
 
	lr_start_transaction("STRA_SP5_02_TestResults");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t12541.inf",
		"Body={\n"
		"\"sortBy\": \"Date\",\n"
		"\"sortOrder\": \"Descending\",\n"
		"\"offset\": 1,\n"
		"\"limit\": 10,\n"
		"\"searchTerm\": \"\",	\n"
		"\"showHospitalResults\": true\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP5_02_TestResults", 2);
	 
	lr_output_message("Test ID info is %s",lr_eval_string("{TID}"));
		lr_think_time(5);

	 
	web_reg_save_param("componentId",
	                   "LB=\"componentId\":\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   "LAST");

 
 	lr_start_transaction("STRA_SP5_03_TestResultsById");
	web_reg_save_param("TestResultsByIdRes", "LB=", "RB=","LAST");		
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults/{TID}",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t246234.inf",
		"Body={\n"
		"\"sortBy\": \"Date\",\n"
		"\"sortOrder\": \"Descending\",\n"
		"\"offset\": 1,\n"
		"\"limit\": 10,\n"
		"\"searchTerm\": \"\",  \n"
		"\"showHospitalResults\": true\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	
	lr_end_transaction("STRA_SP5_03_TestResultsById", 2);
	lr_output_message("Test Results BY ID info is %s",lr_eval_string("{TestResultsByIdRes}"));
	 
		lr_think_time(5);

 

	lr_start_transaction("STRA_SP5_04_TestResultsPAST");
	 
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults/Past",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t264196.inf",
		"Body={\n"
		"\"isGraph\":false,\n"
		"\"componentIds\": [{{COMPONENTID}}],\n"
		 
		"\"sortBy\": \"OldestFirst\",\n"
		"\"selectionType\": \"NumberofResults\",\n"
		"\"filters\": {\n"
		"\"date\": {\n"
		"\"from\": \"2017-01-01\",\n"
		"\"to\": \"2018-01-28\"\n"
		"},\n"
		"\"resultCount\":5\n"
		"}\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP5_04_TestResultsPAST", 2);
	 

		lr_think_time(5);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

	 
	web_reg_save_param("Auth2",
	                   "LB=\"token\":\"",
	                   "RB=\"",
	                   "convert=HTML_TO_URL",
	                   "LAST");
 
	lr_start_transaction("STRA_SP5_06_LucySSO");
	 
	web_rest("GET: {UATURL}/common/v1/Char...",
		"URL={UATURL}/common/v1/Chart/lucysso/68280885E0D14152A5BE6E1D96D011F7",
		"Method=GET",
		"Snapshot=t138743.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP5_06_LucySSO", 2);
	 
	lr_output_message("LucySSO AuthToken info is %s",lr_eval_string("{Auth2}"));
		lr_think_time(5);
 
 
 
 
 
 
 
 
 
 
 
 

	
 
 
lr_start_transaction("STRA_SP5_07_LucySSOAuthorizedToken");

	web_rest("POST: {UATURL}/common/v1/Chart/sso",
		"URL={UATURL}/common/v1/Chart/sso",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t139507.inf",
		"Body={\n"
		"\"token\": \"{Auth2}\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");


lr_end_transaction("STRA_SP5_07_LucySSOAuthorizedToken", 2);
		lr_think_time(5);
 

 
	lr_start_transaction("STRA_SP6_08_Beacon");
	 
	web_rest("GET: {UATURL}/common/v1/Pati...",
		"URL={UATURL}/common/v1/Patient/PatientInformation?deviceId=AA_AA_AA_AA_AAAE",
		"Method=GET",
		"Snapshot=t706111.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP6_08_Beacon", 2);
	 
		lr_think_time(5);
 
	lr_start_transaction("STRA_SP6_09_EcheckinPendingAppointments");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/EcheckinPendingAppointments",
		"Method=GET",
		"Snapshot=t257779.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP6_09_EcheckinPendingAppointments", 2);
	 
		lr_think_time(5);
 
	lr_start_transaction("STRA_SP6_10_IconBadge");
	 
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PatientDashboard",
		"Method=GET",
		"Snapshot=t763865.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_SP6_10_IconBadge", 2);
	lr_output_message("IconBadge info is %s",lr_eval_string("{IconBadgeRes}"));
 
 
lr_start_transaction("STRA_UAT_APPUPGRADE");

	web_rest("POST: {UATURL}/common/v1/Device/AppUpgrade",
		"URL={UATURL}/common/v1/Device/AppUpgrade",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t347820.inf",
		"Body={\n"
		"\"appName\": \"sentaraios\",\n"
		"\"version\": \"2.3\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

lr_end_transaction("STRA_UAT_APPUPGRADE", 2);
 
	web_reg_save_param("FacilityID",
	                   "LB=\"facilityId\":",
	                   "RB=,\"",
	                    "LAST");
 
lr_start_transaction("STRA_UAT_FACILITIES");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities?zipCode=23455&locationType=3&distance=26",
		"URL={UATURL}/common/v1/PatientVisits/Facilities?zipCode=23455&locationType=3&distance=26",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_FACILITIES", 2);
 
lr_start_transaction("STRA_SP3_29_FacilityDetails");
	 
	web_rest("GET: {UATURL}/common/v1/Pa...",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/{FacilityID}",
		"Method=GET",
		"Snapshot=t718694.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");	
	lr_end_transaction("STRA_SP3_29_FacilityDetails", 2);
 
lr_start_transaction("STRA_UAT_CampusMapFACILITIES");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities/CampusMap/Facilities",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/CampusMap/Facilities",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_CampusMapFACILITIES", 2);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
lr_start_transaction("STRA_UAT_Languages");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Languages",
		"URL={UATURL}/common/v1/PatientVisits/Languages",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_Languages", 2);
 
lr_start_transaction("STRA_UAT_MdliveStates");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Mdlive/States",
		"URL={UATURL}/common/v1/PatientVisits/Mdlive/States",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_MdliveStates", 2);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
lr_start_transaction("STRA_UAT_ProviderImage");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/7832/Image",
		"URL={UATURL}/common/v1/PatientVisits/Providers/7832/Image",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_ProviderImage", 2);
 
lr_start_transaction("STRA_UAT_GetVisitReason");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"URL={UATURL}/common/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetVisitReason", 2);
 
lr_start_transaction("STRA_UAT_GetSpecialties");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"URL={UATURL}/common/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetSpecialties", 2);
 
lr_start_transaction("STRA_UAT_GetReviewComments");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/{NPI}/ReviewComments",
		"URL={UATURL}/common/v1/PatientVisits/Providers/{NPI}/ReviewComments",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetReviewComments", 2);
 
lr_start_transaction("STRA_UAT_GetListQuestions");
	web_rest("GET: {UATURL}/common/v1/Questions",
		"URL={UATURL}/common/v1/Questions",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetListQuestions", 2);
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
lr_start_transaction("STRA_UAT_ValidateUserID");
	web_rest("GET: {UATURL}/common/v1/Validation/UserId/{userid}",
		"URL={UATURL}/common/v1/Validation/UserId/{userid}",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_ValidateUserID", 2);
 
lr_start_transaction("STRA_UAT_ValidateUserName");
	web_rest("GET: {UATURL}/common/v1/Validation/Username/{username}",
		"URL={UATURL}/common/v1/Validation/Username/{username}",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_ValidateUserName", 2);
 
lr_start_transaction("STRA_UAT_GetLocationType");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities/LocationTypes",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/LocationTypes",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetLocationType", 2);
 
 
 
 
 
 
 
 
 
 
 
 
 
 
lr_start_transaction("STRA_UAT_GetProviderDetailsOnVtypeAndProviders");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/66?idType=epi",
		"URL={UATURL}/common/v1/PatientVisits/Providers/66?idType=epi",
		"Method=GET",
		"Snapshot=t972900.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
lr_end_transaction("STRA_UAT_GetProviderDetailsOnVtypeAndProviders", 2);
 
lr_start_transaction("STRA_UAT_GetProviders");

	web_rest("POST: {UATURL}/common/v1/PatientVisits/Providers",
		"URL={UATURL}/common/v1/PatientVisits/Providers",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t689317.inf",
		"Body={\n"
		"\"time\": {\n"
		"\"from\": \"\",\n"
		"\"to\": \"\"\n"
		"},\n"
		"\"reasonId\": \"\",\n"
		"\"date\": \"\",\n"
		"\"providerName\": \"\",\n"
		"\"providerType\": \"\",\n"
		"\"gender\": \"\",\n"
		"\"zipcode\": \"{ZIPCODE}\",\n"
		"\"latitude\": \"\",\n"
		"\"longitude\": \"\",\n"
		"\"radius\": 10,\n"
		"\"languages\": \"\",\n"
		"\"specialty\": \"\",\n"
		"\"offset\":\"1\",\n"
		"\"limit\":\"40\",\n"
		"\"showPrimaryCareProviders\": \"true\",\n"
		"\"onlyShowProvidersWhoScheduleOnline\": \"true\",\n"
		"\"showNonSentaraProviders\":\"true\",\n"
		"\"smg\":0\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetProviders", 2);

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


 
	lr_start_transaction("STRA_UAT_retriveAppointmentDetail");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Appointments/2L23iQ%252Bz1Lphy4CzjWSLvw%253D%253D?appointmentType=1",
		"URL={UATURL}/consumer/v1/PatientVisits/Appointments/2L23iQ%252Bz1Lphy4CzjWSLvw%253D%253D?appointmentType=1",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_retriveAppointmentDetail", 2);
 
	lr_start_transaction("STRA_UAT_GetSSOTOKEN");
	web_rest("GET: {UATURL}/consumer/v1/Chart/sso",
		"URL={UATURL}/consumer/v1/Chart/sso",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetSSOTOKEN", 2);
 
	lr_start_transaction("STRA_UAT_GetVirtualCare");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Configurations/VirtualCare",
		"URL={UATURL}/consumer/v1/PatientVisits/Configurations/VirtualCare",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetVirtualCare", 2);

 
	lr_start_transaction("STRA_UAT_GetImagingFacilities");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/imaging/appointment/facilities?visitTypeId=15800&zipCode=23455&distance=5&offset=1&limit=10&imagingType=3&uniqueFacilities=true",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointment/facilities?visitTypeId=15800&zipCode=23455&distance=5&offset=1&limit=10&imagingType=3&uniqueFacilities=true",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetImagingFacilities", 2);
 
	lr_start_transaction("STRA_UAT_GetListOfLanguages");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Languages",
		"URL={UATURL}/consumer/v1/PatientVisits/Languages",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetListOfLanguages", 2);
 
	lr_start_transaction("STRA_UAT_SSOTokForMdlive");
	
	web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Mdlive/SSO",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/SSO",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t312219.inf",
		"Body={\n"
		"\"auth\": {\n"
		"\"first_name\": \"string\",\n"
		"\"last_name\": \"string\",\n"
		"\"gender\": \"string\",\n"
		"\"birthdate\": \"string\",\n"
		"\"subscriber_id\": \"string\",\n"
		"\"member_id\": \"string\",\n"
		"\"phone\": \"string\",\n"
		"\"email\": \"string\",\n"
		"\"address1\": \"string\",\n"
		"\"city\": \"string\",\n"
		"\"state\": \"string\",\n"
		"\"zip\": \"string\",\n"
		"\"relationship\": \"self\",\n"
		"\"primary_first_name\": \"string\",\n"
		"\"primary_last_name\": \"string\",\n"
		"\"primary_gender\": \"string\",\n"
		"\"primary_birthdate\": \"string\",\n"
		"\"primary_subscriber_id\": \"string\",\n"
		"\"primary_member_id\": \"string\",\n"
		"\"primary_address1\": \"string\",\n"
		"\"primary_address2\": \"string\",\n"
		"\"primary_city\": \"string\",\n"
		"\"primary_state\": \"string\",\n"
		"\"primary_zip\": \"string\"\n"
		"},\n"
		"\"org\": {\n"
		"\"ou\": \"1\"\n"
		"},\n"
		"\"api\": {\n"
		"\"api_key\": \"string\",\n"
		"\"password\": \"string\"\n"
		"}\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	
	lr_end_transaction("STRA_UAT_SSOTokForMdlive", 2);
 
	lr_start_transaction("STRA_UAT_ExtendSSOTokForMdlive");
	web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Mdlive/E...",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/ExtendedSSO?ljsjgdflh=",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t993927.inf",
		"Body={\n"
		"\"auth\": \"string\",\n"
		"\"org\": {\n"
		"\"ou\": \"string\"\n"
		"},\n"
		"\"api\": {\n"
		"\"api_key\": \"string\",\n"
		"\"password\": \"string\"\n"
		"}\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_ExtendSSOTokForMdlive", 2);
 
	lr_start_transaction("STRA_UAT_OUInsuranceList");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Mdlive/OUInsurance",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/OUInsurance",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_OUInsuranceList", 2);
 
	lr_start_transaction("STRA_UAT_PersonInsurancesList");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Mdlive/PersonOUInsurance",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/PersonOUInsurance",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_PersonInsurancesList", 2);
 
	lr_start_transaction("STRA_UAT_AlertsCount");
	web_rest("GET: {UATURL}/consumer/v1/Patient/messages/alerts/count",
		"URL={UATURL}/consumer/v1/Patient/messages/alerts/count",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_AlertsCount", 2);
 
	lr_start_transaction("STRA_UAT_SendMessage");
	
		web_rest("POST: {UATURL}/consumer/v1/Patient/SendMessage",
		"URL={UATURL}/consumer/v1/Patient/SendMessage",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t156249.inf",
		"Body={\n"
		"\"body\": \"string\",\n"
		"\"messageType\": \"string\",\n"
		"\"subject\": \"string\",\n"
		"\"sendTo\": \"string\",\n"
		"\"viewers\": [\n"
		"{\n"
		"\"viewer\": \"string\"\n"
		"}\n"
		"]\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	
	lr_end_transaction("STRA_UAT_SendMessage", 2);
 
	lr_start_transaction("STRA_UAT_PatientMedHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/MedicalHistory",
		"URL={UATURL}/consumer/v1/Patient/MedicalHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_PatientMedHistory", 2);
 
	lr_start_transaction("STRA_UAT_PatientSurgeHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/SurgicalHistory",
		"URL={UATURL}/consumer/v1/Patient/SurgicalHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_PatientSurgeHistory", 2);
 
	lr_start_transaction("STRA_UAT_PatientFamilyHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/FamilyHistory",
		"URL={UATURL}/consumer/v1/Patient/FamilyHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_PatientFamilyHistory", 2);
 
	lr_start_transaction("STRA_UAT_PatientGenderInfo");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Gender",
		"URL={UATURL}/consumer/v1/Patient/Gender",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_PatientGenderInfo", 2);
 
		lr_start_transaction("STRA_UAT_SendEmail");
		web_rest("PUT: {UATURL}/consumer/v1/Patient/Profile/email",
		"URL={UATURL}/consumer/v1/Patient/Profile/email",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t376394.inf",
		"Body={\n"
		"\"userEmail\": {\n"
		"\"email\": \"string\"\n"
		"},\n"
		"\"channel\": \"string\",\n"
		"\"lang\": \"string\"\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	web_rest("GET: {UATURL}/consumer/v1/Patient/Gender",
		"URL={UATURL}/consumer/v1/Patient/Gender",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_SendEmail", 2);
 
		lr_start_transaction("STRA_UAT_GetLanguages");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Profile/Languages",
		"URL={UATURL}/consumer/v1/Patient/Profile/Languages",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetLanguages", 2);
 
		lr_start_transaction("STRA_UAT_GetEntitlementsOfUser");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Profile/entitlements",
		"URL={UATURL}/consumer/v1/Patient/Profile/entitlements",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetEntitlementsOfUser", 2);
 
		lr_start_transaction("STRA_UAT_GetOpenSlotsForImagingAppointmentSlotsrollUp");
		web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Provider...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/Imaging/OpenSlots",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t18998.inf",
		"Body={\n"
		"\"visitTypeId\": \"15800\",\n"
		"\"visitType\": \"External\",\n"
		"\"departmentId\": \"20316002\",\n"
		"\"departmentType\": \"External\",\n"
		"\"startDate\": \"{START_DATE}\",\n"
		"\"endDate\": \"{END_DATE}\",\n"
		"\"showNextAvailableSlots\": true,\n"
		"\"distance\": 50\n"
		"}",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");
	
	lr_end_transaction("STRA_UAT_GetOpenSlotsForImagingAppointmentSlotsrollUp", 2);
 

		lr_start_transaction("STRA_UAT_GetNameOfProviders");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/name?providername=rav&offset=1&limit=50&GetCareType=2",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/name?providername=rav&offset=1&limit=50&GetCareType=2",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_GetNameOfProviders", 2);
 
		lr_start_transaction("STRA_UAT_ZoomMeetingInfoBasedOnAppointment");
	web_rest("GET: {UATURL}/consumer/v1/Zoom/MeetingInfo/91UV8%2BA6ce0IDQWrcyj%2BRQ%3D%3D",
		"URL={UATURL}/consumer/v1/Zoom/MeetingInfo/91UV8%2BA6ce0IDQWrcyj%2BRQ%3D%3D",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_UAT_ZoomMeetingInfoBasedOnAppointment", 2);
 
	lr_start_transaction("STRA_SP4_8_Logout");
	web_rest("DELETE: {UATURL}/consumer/v1/User/logout",
		"URL={UATURL}/consumer/v1/User/logout",
		"Method=DELETE",
		"EncType=raw",
		"Snapshot=t792925.inf",
		"HEADERS",
		"Name=Content-Type", "Value=application/json-patch+json", "ENDHEADER",
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", "ENDHEADER",
		"Name=Authorization", "Value={authToken_1}", "ENDHEADER",
		"LAST");

	lr_end_transaction("STRA_SP4_8_Logout", 2);

		return 0;
}
# 5 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

# 1 "vuser_end.c" 1
vuser_end()
{
	return 0;
}
# 6 "c:\\scripts\\sentara_uat_lrupscripts_12042019\\sentara_uat_lrupscripts_10252019\\\\combined_SENTARA_UAT_LRUPSCRIPTS_10252019.c" 2

