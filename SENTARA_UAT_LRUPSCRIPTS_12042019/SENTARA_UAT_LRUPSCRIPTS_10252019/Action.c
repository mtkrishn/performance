Action()
{
	web_set_max_html_param_len("1024");
	web_set_sockets_option("SSL_VERSION", "TLS1.2");
web_reg_save_param("authToken", "LB=\"authToken\":\"", "RB=\",\"user","ORD=ALL",LAST);
	lr_start_transaction("STRA_UAT_01_Login");
	//web_reg_save_param("LoginResponse", "LB=", "RB=",LAST);	
	web_rest("POST: {UATURL}/common/v1/User",
		"URL={UATURL}/common/v1/User",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t940759.inf",
		"Body={\n"
		"\"userId\": \"{USERNAME}\",\n"	
		"\"password\": \"{PASSWORD}\",\n"
		"\"channel\": \"string\",\n"
		"\"lang\": \"string\",\n"
		"\"identification\": {\n"
		"\"deviceId\": \"string\"\n"
		"},\n"
		"\"terms\": \"string\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=DeviceInfo", "Value=DeviceID:PrakashTesting1, AppName: Sentara, AppVersion: 1.0, IP Address:1.1.1.1, MAC Address:", ENDHEADER,
		LAST);
	
	if(atoi(lr_eval_string("{authToken_count}"))>0)
    {
 lr_end_transaction("STRA_UAT_01_Login", LR_PASS);
 lr_output_message("LoginPassed  %s",lr_eval_string("{USERNAME}"));
}
 else
 {
 lr_end_transaction("STRA_SP2_01_Login", LR_FAIL);
 lr_error_message("Login failed,  %s received", lr_eval_string("{USERNAME}"));
 //lr_output_message("The UserName is %s",lr_eval_string("{USERNAME}"));
 lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_FAIL);
 }
	//Allergies
	lr_start_transaction("STRA_UAT_Authenticated_02_Allergies");
	//web_reg_save_param("AllergiesRes", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Allergies",
		"Method=GET",
		"Snapshot=t143629.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	lr_end_transaction("STRA_UAT_Authenticated_02_Allergies", LR_AUTO);
	//lr_output_message("Allergies info is %s",lr_eval_string("{AllergiesRes}"));
	lr_think_time(5);
//CurrentHealthIssues
	lr_start_transaction("STRA_UAT_Authenticated_03_CurrentHealthIssues");
	//web_reg_save_param("CurrentHealthIssuesRes", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/CurrentHealthIssues",
		"Method=GET",
		"Snapshot=t51781.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_UAT_Authenticated_03_CurrentHealthIssues", LR_AUTO);
	//lr_output_message("CurrentHealthIssues info is %s",lr_eval_string("{CurrentHealthIssuesRes}"));
	lr_think_time(5);
//Immunizations
	lr_start_transaction("STRA_UAT_Authenticated_04_Immunizations");
	//web_reg_save_param("ImmunizationsRes", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Immunizations",
		"Method=GET",
		"Snapshot=t323762.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_UAT_Authenticated_04_Immunizations", LR_AUTO);
	//lr_output_message("Immunizations info is %s",lr_eval_string("{ImmunizationsRes}"));
	lr_think_time(5);
//PatientMedications
	lr_start_transaction("STRA_UAT_Authenticated_05_PatientMedications");
	//web_reg_save_param("PatientMedicationsRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Medications",
		"Method=GET",
		"Snapshot=t958799.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_UAT_Authenticated_05_PatientMedications", LR_AUTO);
	//lr_output_message("PatientMedications info is %s",lr_eval_string("{PatientMedicationsRes}"));
	lr_think_time(5);
//MedicalRecord
	lr_start_transaction("STRA_UAT_Authenticated_06_MedicalRecord");
	//web_reg_save_param("MedicalRecordRes", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/MedicalRecord",
		"Method=GET",
		"Snapshot=t931622.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	lr_end_transaction("STRA_UAT_Authenticated_06_MedicalRecord", LR_AUTO);
	//lr_output_message("MedicalRecord info is %s",lr_eval_string("{MedicalRecordRes}"));
	lr_think_time(5);

//HealthSummary
	lr_start_transaction("STRA_UAT_Authenticated_07_HealthSummary");
	//web_reg_save_param("HealthSummaryRes", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/HealthSummary?platform=mobile",
		"Method=GET",
		"Snapshot=t361116.inf",
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	
	lr_end_transaction("STRA_UAT_Authenticated_07_HealthSummary", LR_AUTO);
	lr_think_time(5);
	//lr_output_message("HealthSummary info is %s",lr_eval_string("{HealthSummaryRes}"));

//PreventiveCare API
	lr_start_transaction("STRA_UAT_Authenticated_08_PreventiveCare");
	//web_reg_save_param("PreventiveCareRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PreventiveCare",
		"Method=GET",
		"Snapshot=t435586.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_Authenticated_08_PreventiveCare", LR_AUTO);
//	lr_output_message("PreventiveCareDetail Response is %s",lr_eval_string("{PreventiveCareRes}"));
	lr_think_time(5);
//PCP API 
	lr_start_transaction("STRA_UAT_Authenticated_09_PCP");
	//web_reg_save_param("PCPRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/PCP",
		//"URL={UATURL}/consumer/v1/PatientVisits/Providers/PCP?pcpType={PCP_TYPE}",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/PCP",
		"Method=GET",
		"Snapshot=t505984.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_UAT_Authenticated_09_PCP", LR_AUTO);
	//lr_output_message("PCP Response is %s",lr_eval_string("{PCPRes}"));
	lr_think_time(5);
	
//	//"providerId":"1631","EPI":"
//	web_reg_save_param("EPI_ID",
//	                   "LB=\"EPI\":\"",
//	                   "RB=\"",
//	                   LAST);
//	web_reg_save_param("Provider_ID",
//	                   "LB=\"providerId\":\"",
//	                   "RB=\"",
//	                   LAST);
	//List Of Providers API	
	lr_start_transaction("STRA_UAT_Authenticated_10_Providers");
	//web_reg_save_param("ProvidesListResponse", "LB=", "RB=",LAST);	
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t46687.inf",
		"Body={\n"
		"\"time\": {\n"
		"\"from\": \"\",\n"
		"\"to\": \"\"\n"
		"},\n"
		"\"reasonId\": \"\",\n"
		"\"date\": \"\",\n"
		"\"providerName\": \"\",\n"
		"\"providerType\": \"\",\n"
		"\"gender\": \"\",\n"
		"\"zipcode\": \"{ZIPCODE}\",\n"
		"\"latitude\": \"\",\n"
		"\"longitude\": \"\",\n"
		"\"radius\": 10,\n"
		"\"languages\": \"\",\n"
		"\"specialty\": \"\",\n"
		"\"offset\":\"1\",\n"
		"\"limit\":\"40\",\n"
		"\"showPrimaryCareProviders\": \"true\",\n"
		"\"onlyShowProvidersWhoScheduleOnline\": \"true\",\n"
		"\"showNonSentaraProviders\":\"true\",\n"
		"\"smg\":0\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	
	lr_end_transaction("STRA_UAT_Authenticated_10_Providers", LR_AUTO);
	//lr_output_message("Providers API Response is %s",lr_eval_string("{ProvidesListResponse}"));
	//lr_output_message("AuthToken is %s",lr_eval_string("{authToken}"));
	//lr_output_message("EPI VALUE is %s",lr_eval_string("{EPI_ID}"));
	lr_think_time(5);
//	//"commentsCount":"97","providerId":"1631","doctorImg"
//	web_reg_save_param("Provider_ID",
//	                   "LB=\"providerId\":\"",
//	                   "RB=\"",
//	                   LAST);
//	//"departmentId":20703001}
//	web_reg_save_param("Department_ID",
//	                   "LB=\"departmentId\":",
//	                   "RB=}",
//	                   LAST);
//	//"NPI":"1528033180"
//	web_reg_save_param("NPI_ID",
//	                   "LB=\"NPI\":\"",
//	                   "RB=\"",
//	                   LAST);
//This API is used to retrieve details of the provider
//providerDetails-ProviderById API	
	lr_start_transaction("STRA_UAT_Authenticated_11_ProviderDetails");
	//web_reg_save_param("ProviderDetailsResponse", "LB=", "RB=",LAST);	
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/7832?idType=epi",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/7832?idType=epi",
		"Method=GET",
		"Snapshot=t437254.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_Authenticated_11_ProviderDetails", LR_AUTO);
	//lr_output_message("ProviderDetailsRes is %s",lr_eval_string("{ProviderDetailsResponse}"));
	lr_output_message("ProviderID value is %s",lr_eval_string("{Provider_ID}"));
	lr_output_message("DepartmentIDRes is %s",lr_eval_string("{Department_ID}"));
	lr_output_message("NPI Value is %s",lr_eval_string("{NPI_ID}")); 
	lr_think_time(5);
	
	
	//"date":"2018-09-06"
/*	web_reg_save_param("OPENDATE",
	                   "LB=\"date\":\"",
	                   "RB=\"",
	                   LAST);
	//"appointments":[{"time":"13:00:00"
	web_reg_save_param("TimeStamp",
	                   "LB=\"time\":\"",
	                   "RB=\"",
	                   LAST);
	//"duration":15}
	web_reg_save_param("DurationTime",
	                   "LB=\"duration\":",
	                   "RB=}",
	                   LAST);
*/
//OpenSlots API
	lr_start_transaction("STRA_UAT_Authenticated_12_OpenSlots");
	//web_reg_save_param("OpendslotsResponse", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/OpenSlots",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t881379.inf",
		"Body={\n"
		//"\"providerId\": \"{Provider_ID}\",\n"
		"\"providerId\": \"4221\",\n"
		"\"providerType\": \"External\",\n"
		"\"visitTypeId\": \"1099\",\n"
		"\"visitType\": \"External\",\n"
		//"\"departmentId\": \"{Department_ID}\",\n"
		"\"departmentId\": \"20837003\",\n"
		"\"departmentType\": \"External\",\n"
		"\"startDate\": \"{START_DATE}\",\n"
		"\"endDate\": \"{END_DATE}\",\n"
		"\"showNextAvailableSlots\": true\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	lr_end_transaction("STRA_UAT_Authenticated_12_OpenSlots", LR_AUTO);
	//lr_output_message("openslotsRes is %s",lr_eval_string("{OpendslotsResponse}"));
/*	lr_output_message("opendateres is %s",lr_eval_string("{OPENDATE}"));
	lr_output_message("timeres is %s",lr_eval_string("{TimeStamp}"));
	lr_output_message("Durationtime is %s",lr_eval_string("{DurationTime}"));
	lr_think_time(5);
*/

	//"appoinmentId":"20010010083155"
/*	web_reg_save_param("Appoinment_ID",
	                   "LB=\"appoinmentId\":\"",
	                   "RB=\"",
	                   //"ORD=ALL",
	                   LAST);	*/
//ActiveAppoinments
	lr_start_transaction("STRA_UAT_Authenticated_13_ActiveAppoinments");
	web_reg_save_param("ACTIVEApponmentsRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Appointments?Type=1&offset=1&limit=20",
		"Method=GET",
		"Snapshot=t4250.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_Authenticated_13_ActiveAppoinments", LR_AUTO);
	//lr_output_message("ActiveAppoinmentRes is %s",lr_eval_string("{ACTIVEApponmentsRes}"));
	//lr_output_message("ActiveAppoinmentIDRes is %s",lr_eval_string("{Appoinment_ID}"));
	lr_think_time(5);

//Not used in ALL platforms	
	//UpdateWaitList,Hence,Commented in scripting
//	lr_start_transaction("STRA_UAT_Authenticated_14_UpdateWaitList");
//	//web_reg_save_param("UpdatewaitListRes", "LB=", "RB=",LAST);
//	web_rest("PUT: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/PatientVisits/Appointments/Waitlist",
//		"Method=PUT",
//		"EncType=raw",
//		"Snapshot=t784776.inf",
//		"Body={\n"
//		"\"appointmentNotes\": [ ],\n"
//		"\"contactID\": \"{Appoinment_ID1}\",\n"
//		"\"contactIDType\": \"CSN\",\n"
//		"\"visitTypeID\": \"{VisitTypeId}\",\n"
//		"\"visitType\": \"External\",\n"
//		"\"treatmentMethod\": \"\",\n"
//		"\"providerID\": \"{DOCTOR_ID}\",\n"
//		"\"providerType\": \"External\",\n"
//		"\"departmentID\": \"{DepartmentID1}\",\n"
//		"\"departmentType\": \"External\",\n"
//		"\"date\": \"{OpenDate1}\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_UAT_Authenticated_14_UpdateWaitList", LR_AUTO);
//	//lr_output_message("Updated waiting List Response is %s",lr_eval_string("{UpdatewaitListRes}"));
//	lr_think_time(5);
/*	
	//"departmentId":"20703001"
	web_reg_save_param("DepartmentID1",
	                   "LB=\"departmentId\":\"",
	                   "RB=\"",
	                   LAST);
	//"visitTypeId":"1099"
	web_reg_save_param("VisitTypeId",
	                   "LB=\"visitTypeId\":\"",
	                   "RB=\"",
	                   LAST);

	//"date":"09-06-2018 13:15:00"
	web_reg_save_param("{OpenDate1}",
	                   "LB=\"date\":\"",
	                   "RB=\"",
	                   LAST);
	//{"doctorId":"1631"
	web_reg_save_param("DOCTOR_ID",
	                   "LB=\"doctorId\":\"",
	                   "RB=\"",
	                   LAST);
	
*/

//Providers-VisitType

	lr_start_transaction("STRA_SP3_15_ProviderVisitType");
	//web_reg_save_param("ProviderVisitType", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		//"URL={UATURL}/consumer/v1/PatientVisits/Providers/Specialties?searchTerm=Anesthesiology",
		//"URL={UATURL}/consumer/v1/PatientVisits/Providers/Specialties",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"Method=GET",
		"Snapshot=t381117.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP3_15_ProviderVisitType", LR_AUTO);
	//lr_output_message("ProviderVisitType Response is %s",lr_eval_string("{ProviderVisitType}"));
	lr_think_time(5);

//VisitType-Reason by Established API
	lr_start_transaction("STRA_SP3_16_VisitTypeEstablished");
	//web_reg_save_param("VisitTypeEstablishedRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/Established/Reasons",
		"Method=GET",
		"Snapshot=t124016.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP3_16_VisitTypeEstablished", LR_AUTO);
	//lr_output_message("ProviderVisitTypeEstablioshed Response is %s",lr_eval_string("{VisitTypeEstablishedRes}"));
	lr_think_time(5);
	
//VisitType-Reason by New API
	lr_start_transaction("STRA_SP3_17_VisitTypeNew");
	//web_reg_save_param("VisitTypeNewRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"Method=GET",
		"Snapshot=t157300.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP3_17_VisitTypeNew", LR_AUTO);
	//lr_output_message("VisitTypeNew Response is %s",lr_eval_string("{VisitTypeNewRes}"));
	lr_think_time(5);

//VisitType-Reason by VirtualCare API
	lr_start_transaction("STRA_SP3_18_VisitTypeVirtualCare");
	//web_reg_save_param("VisitTypeVirtualCareRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/VirtualCare/Reasons",
		"Method=GET",
		"Snapshot=t732626.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP3_18_VisitTypeVirtualCare", LR_AUTO);
	//lr_output_message("VisitTypeVirtualCare Response is %s",lr_eval_string("{VisitTypeVirtualCareRes}"));
	lr_think_time(5);
//VisitType-Reason by Sameday API
	lr_start_transaction("STRA_SP3_19_VisitTypeSameday");
	//web_reg_save_param("VisitTypeSamedayRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/VisitType/SameDay/Reasons",
		"Method=GET",
		"Snapshot=t247965.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP3_19_VisitTypeSameday", LR_AUTO);
	//lr_output_message("ProviderVisitTypeSameday Response is %s",lr_eval_string("{VisitTypeSamedayRes}"));
	lr_think_time(5);
	
	//"AccessList":[{"AccessClass":{"ID":"100502"

//	web_reg_save_param("Proxy_ID",
//	                   "LB=\"ID\":\"",
//	                   "RB=\"",
//	                   LAST);


//GET_Proxy API
//	lr_start_transaction("STRA_SP3_23_Proxy");
//	//web_reg_save_param("ProxyRes", "LB=", "RB=",LAST);
//	web_rest("GET: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/Patient/Proxy",
//		"Method=GET",
//		"Snapshot=t870925.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_SP3_23_Proxy", LR_AUTO);
//	//lr_output_message("Proxy Response is %s",lr_eval_string("{ProxyRes}"));
//	//lr_output_message("ProxyID value is %s",lr_eval_string("{Proxy_ID}"));
//	lr_think_time(5);
//	
	
//Proxy is not applicable in production,Hence we commented 
////PUT_Proxy
//	lr_start_transaction("STRA_SP3_24_Proxy");
//	//web_reg_save_param("ProxyRes", "LB=", "RB=",LAST);
//	web_rest("PUT: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/Patient/Proxy",
//		"Method=PUT",
//		"EncType=raw",
//		"Snapshot=t355886.inf",
//		"Body={\n"
//		"\"proxyId\": \"{Proxy_ID}\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_SP3_24_Proxy", LR_AUTO);
//	//lr_output_message("Proxy Response is %s",lr_eval_string("{ProxyRes}"));
//	lr_think_time(5);
//
//lr_think_time(5);


	//{"locationTypeId":1,"displayName":
	web_reg_save_param("LocationID",
	                   "LB=\"locationTypeId\":",
	                   "RB=,\"",
	                   "ORD=8",
	                   LAST);
//LocationType API
	lr_start_transaction("STRA_SP3_27_LocationType");
	//web_reg_save_param("LocationTypeRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Facilities/LocationTypes",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities/LocationTypes",
		"Method=GET",
		"Snapshot=t569225.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_27_LocationType", LR_AUTO);
	//lr_output_message("LocationTypeRes Response is %s",lr_eval_string("{LocationTypeRes}"));
	lr_output_message("LocationId value is %s",lr_eval_string("{LocationID}"));
	lr_think_time(5);

	//{"facilities":[{"facilityId":37760,"facilityName"
	web_reg_save_param("FacilityID",
	                   "LB=\"facilityId\":",
	                   "RB=,\"",
	                    LAST);
	                    
//FindAFacility API
	lr_start_transaction("STRA_SP3_28_FindFacility");
	//web_reg_save_param("FindFacilityRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities?zipCode=23462&locationType={LocationID}&distance=50",
		"Method=GET",
		"Snapshot=t548567.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_28_FindFacility", LR_AUTO);
	//lr_output_message("Findfacility Response is %s",lr_eval_string("{FindFacilityRes}"));
	lr_output_message("Findfacility ID value is %s",lr_eval_string("{FacilityID}"));
	lr_think_time(5);
//FacilityDetails API
	lr_start_transaction("STRA_SP3_29_FacilityDetails");
	//web_reg_save_param("FinfFacilityRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Facilities/{FacilityID}",
		"Method=GET",
		"Snapshot=t718694.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_29_FacilityDetails", LR_AUTO);
	//lr_output_message("FinfFacility Response is %s",lr_eval_string("{FinfFacilityRes}"));
	lr_think_time(5);
//ReviewComment API
	lr_start_transaction("STRA_SP3_30_ReviewComment");
	//web_reg_save_param("ReviewCommentRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/1528033180/ReviewComments?limit=20&offset=1",
		"Method=GET",
		"Snapshot=t42572.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_30_ReviewComment", LR_AUTO);
	//lr_output_message("ReviewComment Response is %s",lr_eval_string("{ReviewCommentRes}"));
	lr_think_time(5);
//ProviderImage API
//https://webapi.sentara.com/uat1/consumer/v1/PatientVisits/Providers/8839/Image
	lr_start_transaction("STRA_SP3_31_ProviderImage");
	//web_reg_save_param("ProviderImageRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/8839/Image",
		//"URL={UATURL}/consumer/v1/PatientVisits/Providers/{Provider_ID}/Image",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/8839/Image",
		"Method=GET",
		"Snapshot=t197389.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_31_ProviderImage", LR_AUTO);
	//lr_output_message("ProviderImage Response is %s",lr_eval_string("{ProviderImageRes}"));
		
//OrderStatus

	lr_start_transaction("STRA_SP4_02_OrderStatus");
	//web_reg_save_param("Orderstatus", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/orders/status",
		"Method=GET",
		"Snapshot=t28307.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_02_OrderStatus", LR_AUTO);
	//lr_output_message("Order status is %s",lr_eval_string("{Orderstatus}"));

	lr_think_time(3);
	//"flowsheetId":"44250878"
	web_reg_save_param("flowsheetId", "LB=\"flowsheetId\":\"", "RB=\"",LAST);
	//"flowsheetIdType":"INTERNAL"
	web_reg_save_param("flowsheetIdType", "LB=\"flowsheetIdType\":\"", "RB=\"",LAST);
	//"startDate":"2018-07-12",
	web_reg_save_param("StartDate", "LB=\"startDate\":\"", "RB=\"",LAST);
//HealthTracks
	lr_start_transaction("STRA_SP4_03_HealthTracks");
	//web_reg_save_param("HtrackResponse", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"Method=GET",
		"Snapshot=t527050.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP4_03_HealthTracks", LR_AUTO);
	//lr_output_message("TotalRes1 %s",lr_eval_string("{HtrackResponse}"));
	lr_think_time(3);


//HealthTrackDetail
	lr_start_transaction("STRA_SP4_04_HealthTracksDetails");
	//web_reg_save_param("HtrackDetailResponse", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t588550.inf",
		"Body={\n"
		"\"flowsheetId\": \"{flowsheetId}\",\n"
		"\"flowsheetIdType\": \"{flowsheetIdType}\",\n"
		"\"startDate\": \"{StartDate}\",\n"
		"\"endDate\": \"\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_04_HealthTracksDetails", LR_AUTO);
	//lr_output_message("TotalRes1 %s",lr_eval_string("{HtrackDetailResponse}"));
	lr_think_time(3);
	//{"logId":"44248303"
	web_reg_save_param("Logid",
	                   "LB=\"logId\":\"",
	                   "RB=\"",
	                   LAST);
//LogTypes API
	lr_start_transaction("STRA_SP4_05_LogTypes");
	//web_reg_save_param("LogTypeResponse", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes",
		"Method=GET",
		"Snapshot=t867331.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_05_LogTypes", LR_AUTO);
//	lr_output_message("LogTypes is %s",lr_eval_string("{LogTypeResponse}"));
//	lr_output_message("LogID is %s",lr_eval_string("{Logid}"));
	lr_think_time(3);
/*
////44250878
////AddLogApi
//	lr_start_transaction("STRA_SP4_06_AddLogApi");
//	//web_reg_save_param("AddLogResponse", "LB=", "RB=",LAST);
//	web_rest("POST: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logs",
//		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logs",
//		"Method=POST",
//		"EncType=raw",
//		"Snapshot=t959186.inf",
//		"Body={\n"
//		"\"episodeId\": \"{Logid}\",\n"
//		//"\"episodeId\": \"44250878\",\n"
//		"\"episodeIdType\": \"INTERNAL\",\n"
//		"\"readings\": {\n"
//		"\"flowsheetReading\": [\n"
//		"{\n"
//		"\"customListValue\": {\n"
//		"\"abbreviation\": \"\",\n"
//		"\"value\": \"\"\n"
//		"},\n"
//		"\"dataType\": \"\",\n"
//		"\"externalSource\": \"\",\n"
//		"\"instantEntered\": \"2018-05-17T07:35:40.155Z\",\n"
//		"\"instantTaken\": \"2018-05-18T09:35:40.155Z\",\n"
//		"\"isAbnormal\": \"\",\n"
//		"\"isEditable\": \"\",\n"
//		"\"line\": \"\",\n"
//		"\"looseMetadata\": {},\n"
//		"\"numericValue\": \"138\",\n"
//		"\"rowID\": \"24161\",\n"
//		"\"strictMetadata\": {},\n"
//		"\"stringValue\": \"\",\n"
//		"\"valueType\": {\n"	
//		"\"number\": \"\",\n"
//		"\"title\": \"\"\n"
//		"},\n"
//		"\"whyAbnormal\": \"\",\n"
//		"\"originalInstantTaken\": \"\"\n"
//		"}\n"
//		"]\n"
//		"},\n"
//		"\"patientID\": \"\",\n"
//		"\"patientIDType\": \"\",\n"
//		"\"myChartAccountID\": \"\",\n"
//		"\"myChartAccountIDType\": \"\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_SP4_06_AddLogApi", LR_AUTO);
//	//lr_output_message("AddLog is %s",lr_eval_string("{AddLogResponse}"));
//	lr_think_time(3);
////LogDetails API
//	lr_start_transaction("STRA_SP4_07_LogDetails");
//	//web_reg_save_param("LogDetailsResponse", "LB=", "RB=",LAST);
//	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes/{Logid}",
//		"URL={UATURL}/consumer/v1/Patient/HealthRecords/HealthTracks/logTypes/{Logid}",
//		"Method=GET",
//		"Snapshot=t969540.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_SP4_07_LogDetails", LR_AUTO);
//	//lr_output_message("LogDetails info is %s",lr_eval_string("{LogDetailsResponse}"));
//	lr_think_time(3);
//*/
	//"messageId":"19808691"
	web_reg_save_param("SMID",
	                   "LB=\"messageId\":\"",
	                   "RB=\"",
	                   LAST);
//SentMessages API
	lr_start_transaction("STRA_SP4_08_SendMessage");
	//web_reg_save_param("SENDMSGResponse", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/sentMessages?limit=20",
		"Method=GET",
		"Snapshot=t473769.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_08_SendMessage", LR_AUTO);
	lr_think_time(3);
//	lr_output_message("Send Message info is %s",lr_eval_string("{SENDMSGResponse}"));
//	lr_output_message("Message id is %s",lr_eval_string("{SMID}"));
//SendMessageDetails
	lr_start_transaction("STRA_SP4_09_SendMessageDetail");
	//web_reg_save_param("SENDMSGDETAILResponse", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/sentMessages/{SMID}",
		"Method=GET",
		"Snapshot=t904766.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_09_SendMessageDetail", LR_AUTO);
	//lr_output_message("Send Message info is %s",lr_eval_string("{SENDMSGDETAILResponse}"));
//ContactDoctor API
	lr_start_transaction("STRA_SP4_10_ContactDoctor");
	//web_reg_save_param("ContactDocResponse", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/patient/messages/contactDoctor",
		"Method=GET",
		"Snapshot=t95798.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_10_ContactDoctor", LR_AUTO);
	lr_think_time(3);
	//lr_output_message("Doctor contact info is %s",lr_eval_string("{ContactDocResponse}"));

//HealthTrackGraph
	lr_start_transaction("STRA_SP4_11_HealthTrackGraph");
	//web_reg_save_param("HTGraphResponse", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Patient/HealthRecords/healthTracks/graph",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/healthTracks/graph",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t639981.inf",
		"Body={\n"
		"\"flowsheetId\": \"{flowsheetId}\",\n"
		"\"flowsheetIdType\": \"INTERNAL\",\n"
		"\"startDate\": \"2018-07-12\",\n"
		//"\"endDate\": \"{{endDate}}\",\n"
		"\"componentIds\": null\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_11_HealthTrackGraph", LR_AUTO);
	lr_think_time(3);
	//lr_output_message("HTGraph info is %s",lr_eval_string("{HTGraphResponse}"));
	
	//"False","id":"19808878",
	web_reg_save_param("Mid",
	                   "LB=\"False\",\"id\":\"",
	                   "RB=\"",
	                   LAST);
//Message
	lr_start_transaction("STRA_SP4_12_Message");
	//web_reg_save_param("MIDRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages",
		"Method=GET",
		"Snapshot=t685063.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_12_Message", LR_AUTO);
//	lr_output_message("Message info is %s",lr_eval_string("{MIDRES}"));
//	lr_output_message("MID is %s",lr_eval_string("{Mid}"));
	lr_think_time(3);
//MessageDetails
	lr_start_transaction("STRA_SP4_13_MessageDetails");
	//web_reg_save_param("MDetailRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/Messages/{Mid}",
		"URL={UATURL}/consumer/v1/Patient/Messages/{Mid}",
		"Method=GET",
		"Snapshot=t255618.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_13_MessageDetails", LR_AUTO);
	//lr_output_message("MessageDetail info is %s",lr_eval_string("{MDetailRES}"));
	lr_think_time(3);
	

/*
//DeleteMessage API
	lr_start_transaction("STRA_SP4_14_DeleteMessage");
	web_reg_save_param("DeleteMessageRES", "LB=", "RB=",LAST);
	web_rest("DELETE: {UATURL}/consumer/v1/me...",
		"URL={UATURL}/consumer/v1/messages/{Mid}",
		"Method=DELETE",
		"EncType=raw",
		"Snapshot=t197885.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_14_DeleteMessage", LR_AUTO);
	lr_output_message("Delete Message info is %s",lr_eval_string("{DeleteMessageRES}"));
*/



//WalletCard
	lr_start_transaction("STRA_SP4_15_WalletCard");
	//web_reg_save_param("WalletCardRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/walletCard",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/walletCard",
		"Method=GET",
		"Snapshot=t396856.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_15_WalletCard", LR_AUTO);
	//lr_output_message("Wallet Card info is %s",lr_eval_string("{WalletCardRES}"));
	lr_think_time(3);
//OrdersExpired	
	lr_start_transaction("STRA_SP4_16_OrderExpired");
	//web_reg_save_param("ExporedOrderRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/orders/2/1/5",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/orders/2/1/5",
		"Method=GET",
		"Snapshot=t934448.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_16_OrderExpired", LR_AUTO);
	//lr_output_message("Expired Order info is %s",lr_eval_string("{ExporedOrderRES}"));
	lr_think_time(3);

//ContactCustomerService
	lr_start_transaction("STRA_SP4_17_ContactCustomerService");
	//web_reg_save_param("CCServiceRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages/customerService",
		"Method=GET",
		"Snapshot=t55893.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_17_ContactCustomerService", LR_AUTO);
	//lr_output_message("Customer Contact info is %s",lr_eval_string("{CCServiceRES}"));
	lr_think_time(3);
//NewMessage
	lr_start_transaction("STRA_SP4_18_NewMessage");
	//web_reg_save_param("NewMsgRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Patient/me...",
		"URL={UATURL}/consumer/v1/Patient/messages",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t909718.inf",
		ITEMDATA,
		"Name=To", "Value=P10443", ENDITEM,
		"Name=Subject", "Value=Contact Customer Service6:26", ENDITEM,
		"Name=MessageType", "Value=14", ENDITEM,
		"Name=Body", "Value=Test Body", ENDITEM,
		"Name=Attachments", "FileName=", ENDITEM,
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_18_NewMessage", LR_AUTO);
	//lr_output_message("NewMessage info is %s",lr_eval_string("{NewMsgRES}"));
	lr_think_time(3);
//Orders-Open
	//https://webapidevqa.sentara.com/qa4/consumer/v1/HealthRecord/orders/{{OStatusId}}/{{Expiredoffset}}/{{OLimit}}",
	lr_start_transaction("STRA_SP4_19_OpenOrder");
	//web_reg_save_param("OpenOrderRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/orders/1/1/5",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/orders/1/1/5",
		"Method=GET",
		"Snapshot=t201472.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_19_OpenOrder", LR_AUTO);
	//lr_output_message("Open Order info is %s",lr_eval_string("{OpenOrderRES}"));
	lr_think_time(3);
//WalletCard-Contact Info
	lr_start_transaction("STRA_SP4_20_WalletCardinfo");
	//web_reg_save_param("WalletcontactRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Patient/HealthRecords/WalletCard/ContactInfo",
		"URL={UATURL}/consumer/v1/Patient/HealthRecords/WalletCard/ContactInfo",
		//"URL={UATURL}/consumer/v1/Patient/HealthRecords/WalletCard",
		"Method=GET",
		"Snapshot=t598266.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_20_WalletCardinfo", LR_AUTO);
	//lr_output_message("Wallet Card contact info is %s",lr_eval_string("{WalletcontactRES}"));
	lr_think_time(3);
//TestType
	lr_start_transaction("STRA_SP4_21_TestType");
	//web_reg_save_param("TestRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/patient/HealthRecords/HealthTracks/tests/{flowsheetId}/{flowsheetIdType}",
		"URL={UATURL}/consumer/v1/patient/HealthRecords/HealthTracks/tests/{flowsheetId}/{flowsheetIdType}",
		"Method=GET",
		"Snapshot=t50491.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_21_TestType", LR_AUTO);
	//lr_output_message("Type Of Test info is %s",lr_eval_string("{TestRES}"));
	lr_think_time(3);
//UpdateEmail
	lr_start_transaction("STRA_SP4_22_UpdateEmail");
	//web_reg_save_param("UpdateEmailRES", "LB=", "RB=",LAST);
	web_rest("PUT: {UATURL}/consumer/v1/Patient/Profile/Demographics",
		"URL={UATURL}/consumer/v1/Patient/Profile/Demographics",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t370909.inf",
		"Body={\n"
		"\"demographics\": {\n"
		"\"homeaddress\": {\n"
		"\"houseNumber\": \"\",\n"
		"\"streetAddress\": [\n"
		"\"800 INDEPENDENCE BLVD\"\n"
		"],\n"
		"\"city\": \"VIRGINIA BEACH\",\n"
		"\"state\": \"VA\",\n"
		"\"zipCode\": \"23455-6011\",\n"
		"\"county\": \"Virginia Beach City\",\n"
		"\"country\": \"US\",\n"
		"\"district\": \"\"\n"
		"},\n"
		"\"language\": \"English\",\n"
		"\"emails\": [\n"
		"\"kollaradhakrish@photoninfotech.net\"\n"
		"],\n"
		"\"phoneNumbers\": [\n"
		"{\n"
		"\"phone\": \"757-222-4545\",\n"
		"\"type\": \"mobile\"\n"
		"},\n"
		"{\n"
		"\"phone\": \"000-000-0000\",\n"
		"\"type\": \"Home Phone\"\n"
		"}\n"
		"]\n"
		"}\n"
		"}  \n",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_22_UpdateEmail", LR_AUTO);
	//lr_output_message("Email update info is %s",lr_eval_string("{UpdateEmailRES}"));
	lr_think_time(3);
//UpdateQuestions
	lr_start_transaction("STRA_SP4_23_UpdateQuestions");
	//web_reg_save_param("UpdateQuesRES", "LB=", "RB=",LAST);
	web_rest("PUT: {UATURL}/consumer/v1/Qu...",
		"URL={UATURL}/consumer/v1/Questions",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t881888.inf",
		"Body={"
		"\"questions\": ["
		"{"
		"\"question\": \"What elementary school did you attend?\","
		"\"answer\": \"GMI\""
		"},"
		"{"
		"\"question\": \"What was the model of your first car?\","
		"\"answer\": \"Renault\""
		"},"
		"{"
		"\"question\": \"Who was your childhood hero?\","
		"\"answer\": \"Popoye\""
		"}"
		"]"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_23_UpdateQuestions", LR_AUTO);
	//lr_output_message("Update Questions info is %s",lr_eval_string("{UpdateQuesRES}"));
	lr_think_time(3);

//ForgotPasswordHelp-TriggerEmail
	lr_start_transaction("STRA_SP4_24_FPHTriEmail");
	//web_reg_save_param("FPHTriEmailRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/password",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t701670.inf",
		"Body={"
		"\"user\": {"
		"\"userId\": \"{USERNAME}\""
		"},"
		"\"channel\": \"Web\","
		"\"lang\": \"en\""
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_24_FPHTriEmail", LR_AUTO);
	//lr_output_message("Forgot pass help trigger email info is %s",lr_eval_string("{FPHTriEmailRES}"));
	lr_think_time(3);
	//"token": "eXDHNgJzSVDz66ktNEJtvLemMxdjJCRs/9AOxXzJ06M="
	//web_reg_save_param("authToken_11", "LB=\"authToken_1\":\"", "RB=\",\"user",LAST);
	
	
//ForgotUserName-VerifyUser
	lr_start_transaction("STRA_SP4_25_FogUNameVerifyUser");
	//web_reg_save_param("FOGOTUSERVERIFYUSERRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/verify",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t176171.inf",
		"Body={"
		"\"ssn\": \"5317\","
		"\"dob\": \"1954-12-20\","
		"\"email\": \"mtkrishn@sentara.com\","
		"\"channel\": \"Web\","
		"\"lang\": \"en\""
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_25_FogUNameVerifyUser", LR_AUTO);
	//lr_output_message("VerifyUserName info is %s",lr_eval_string("{FOGOTUSERVERIFYUSERRES}"));
	//lr_output_message("authToken_1 is %s",lr_eval_string("{authToken_11}"));
	lr_think_time(3);
	
//ForgotUserName-StepUp
	lr_start_transaction("STRA_SP4_26_FogUNStepUp");
	//web_reg_save_param("FogUNStepUpRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/common/v1/Use...",
		"URL={UATURL}/common/v1/User/stepup",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t434210.inf",
		"Body={\n"
		"\"token\": \"{authToken_1}\",\n"
		"\"secretQuestion\": \"Who was your childhood hero?\",\n"
		"\"secretAnswer\": \"CaptainAmerica\",\n"
		"\"channel\": \"Web\",\n"
		"\"lang\": \"en\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_26_FogUNStepUp", LR_AUTO);
	//lr_output_message("Forgot User Name StepUp info is %s",lr_eval_string("{FogUNStepUpRES}"));
	lr_think_time(3);
//GetAllQuestions
	lr_start_transaction("STRA_SP4_27_GetAllQuestions");
	//web_reg_save_param("GetAllQuest", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/common/v1/Questions",
		"URL={UATURL}/common/v1/Questions",
		"Method=GET",
		"Snapshot=t427331.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_27_GetAllQuestions", LR_AUTO);
	//lr_output_message("GET all ques info is %s",lr_eval_string("{GetAllQuest}"));
	lr_think_time(3);

//GetProfile
	lr_start_transaction("STRA_SP4_29_GetProfile");
	//web_reg_save_param("GetProfileRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile",
		"Method=GET",
		"Snapshot=t461914.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_29_GetProfile", LR_AUTO);
	//lr_output_message("GetProfile info is %s",lr_eval_string("{GetProfileRES}"));
	lr_think_time(3);


	//"token": "MtqKkvHURVSapBvB9D6DUSnI0YPu+Zlb/8BhNwKuxoCcO8vk6ISTTTQoMWLr+8Qmux+JanhelrcU818lmIkHjA=="
	//web_reg_save_param("encauthToken_1", "LB=\":{\"token\":\"", "RB=\"},",LAST);
//RequestForSSOToken
	lr_start_transaction("STRA_SP4_34_RequestForSSOToken");
	//web_reg_save_param("RequestForSSOTokenRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Ch...",
		"URL={UATURL}/consumer/v1/Chart/sso",
		"Method=GET",
		"Snapshot=t541683.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_34_RequestForSSOToken", LR_AUTO);
	//lr_output_message("RequestForSSOToken info is %s",lr_eval_string("{RequestForSSOTokenRES}"));
	//lr_output_message("EncToken is %s",lr_eval_string("{encauthToken_1}"));
	lr_think_time(3);

//CheckPatientexisttrueW
	lr_start_transaction("STRA_SP4_36_CheckPatientexisttrueW");
	//web_reg_save_param("CheckPatientexisttrueWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PatientExistence",
		"Method=GET",
		"Snapshot=t198733.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_36_CheckPatientexisttrueW", LR_AUTO);
	//lr_output_message("CheckPatientexisttrueW info is %s",lr_eval_string("{CheckPatientexisttrueWRES}"));
	lr_think_time(3);

	//{"id":1,"name":"
	web_reg_save_param("VisitTypeId",
	                   "LB=\"id\":",
	                   "RB=,\"",
	                   LAST);
//ImageVisitTypeW
	lr_start_transaction("STRA_SP4_39_ImageVisitTypeW");
	//web_reg_save_param("ImageVisitTypeWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/visittypes",
		"Method=GET",
		"Snapshot=t852857.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_39_ImageVisitTypeW", LR_AUTO);
//	lr_output_message("ImageVisitTypeW info is %s",lr_eval_string("{ImageVisitTypeWRES}"));
//	lr_output_message(" VisitTypeId is %s",lr_eval_string("{VisitTypeId}"));
	lr_think_time(3);
//ImagingQuestionnaireW
	lr_start_transaction("STRA_SP4_40_ImagingQuestionnaireW");
	//web_reg_save_param("ImagingQuestionnaireWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/visittypes/{VisitTypeId}/questionnaire",
		"Method=GET",
		"Snapshot=t658115.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_40_ImagingQuestionnaireW", LR_AUTO);
	//lr_output_message("ImagingQuestionnaireW info is %s",lr_eval_string("{ImagingQuestionnaireWRES}"));
	lr_think_time(3);
	
//CountryW
	lr_start_transaction("STRA_SP4_41_CountryW");
	//web_reg_save_param("CountryWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/Country",
		"Method=GET",
		"Snapshot=t265796.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_41_CountryW", LR_AUTO);
	//lr_output_message("CountryWRES info is %s",lr_eval_string("{CountryWRES}"));
	lr_think_time(3);
//CountyW
	lr_start_transaction("STRA_SP4_42_CountyW");
	//web_reg_save_param("CountyWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/County",
		"Method=GET",
		"Snapshot=t892501.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_42_CountyW", LR_AUTO);
	//lr_output_message("CountyW info is %s",lr_eval_string("{CountyWRES}"));
	lr_think_time(3);
//StatesW
	lr_start_transaction("STRA_SP4_43_StatesW");
	//web_reg_save_param("StatesWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/States",
		"Method=GET",
		"Snapshot=t368305.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_43_StatesW", LR_AUTO);
	//lr_output_message("StatesW info is %s",lr_eval_string("{StatesWRES}"));
	lr_think_time(3);
/*
//UpLoadProxyImage
	lr_start_transaction("STRA_SP4_44_UpLoadProxyImage");
	web_reg_save_param("UpLoadProxyImageRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		//"URL={UATURL}/consumer/v1/Patient/Profile/Proxy/Image?proxyId={{proxyId}}",
		"URL={UATURL}/consumer/v1/Patient/Profile/Proxy/Image?proxyId=100502",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t925402.inf",
		ITEMDATA,
		"Name=image", "Value=image", ENDITEM,
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_44_UpLoadProxyImage", LR_AUTO);
	lr_output_message("UpLoadProxyImage info is %s",lr_eval_string("{UpLoadProxyImageRES}"));

//ProxyImageW
	lr_start_transaction("STRA_SP4_45_ProxyImageW");
	web_reg_save_param("ProxyImageWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		//"URL={UATURL}/consumer/v1/Patient/Profile/Proxy/{{proxyId}}/Image",
		"URL={UATURL}/consumer/v1/Patient/Profile/Proxy/100502/Image",
		"Method=GET",
		"Snapshot=t617000.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_45_ProxyImageW", LR_AUTO);
	lr_output_message("ProxyImageW info is %s",lr_eval_string("{ProxyImageWRES}"));
	lr_think_time(3);
*/
////Proxy is not applicable in production,Hence we commented 
////Proxy preferences NW
//	lr_start_transaction("STRA_SP4_46_ProxypreferencesNW");
//	//web_reg_save_param("ProxypreferencesNWRES", "LB=", "RB=",LAST);
//	web_rest("PUT: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/Patient/Profile/Proxy/preferences?proxyId=86102325&nickName=yadav&avatarColor=#C0C0C0",
//		"Method=PUT",
//		"EncType=raw",
//		"Snapshot=t816509.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	lr_end_transaction("STRA_SP4_46_ProxypreferencesNW", LR_AUTO);
//	//lr_output_message("Proxy preferences NW info is %s",lr_eval_string("{ProxypreferencesNWRES}"));
	lr_think_time(3);
/********************/
/*//Demographics NW
	lr_start_transaction("STRA_SP4_47_DemographicsNW");
	//web_reg_save_param("DemographicsNWRES", "LB=", "RB=",LAST);
	web_rest("PUT: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/Demographics",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t286636.inf",
		"Body={\n"
		"\"demographics\": {\n"
		"\"homeaddress\": {\n"
		"\"houseNumber\": \"813\",\n"
		"\"streetAddress\": [\n"
		"\"Broad Meadows Ct\"\n"
		"],\n"
		"\"city\": \"Virginia Beach\",\n"
		"\"state\": \"VA\",\n"
		"\"zipCode\": \"23462\",\n"
		"\"county\": \"\",\n"
		"\"country\": \"\",\n"
		"\"district\": \"\"\n"
		"},\n"
		"\"language\": \"\",\n"
		"\"emails\": [\n"
		"\"abc@gmail.com\"\n"
		"],\n"
		"\"phoneNumbers\": [\n"
		"{\n"
		"\"phone\": \"415-688-5044\",\n"
		"\"type\": \"mobile\"\n"
		"}\n"
		"]\n"
		"}\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_47_DemographicsNW", LR_AUTO);
	//lr_output_message("Demographics NW info is %s",lr_eval_string("{DemographicsNWRES}")); 

	//Get Facilities base on EPIC ID NW
	lr_start_transaction("STRA_SP4_48_GetFacilitiesbaseonEPICIDNW");
	web_reg_save_param("GetFacilitiesbaseonEPICIDNWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointment/visittype/15800/facilities?zipcode={ZIPCODE}&offset=1&limit=5",
		"Method=GET",
		"Snapshot=t919135.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_48_GetFacilitiesbaseonEPICIDNW", LR_AUTO);
	lr_output_message("Get Facilities base on EPIC ID NW info is %s",lr_eval_string("{GetFacilitiesbaseonEPICIDNWRES}"));
	lr_think_time(3);
//Get Facilities based on distance W
	lr_start_transaction("STRA_SP4_49_GetFacilitiesBasedOnDistanceW");
	//web_reg_save_param("GetFacilitiesBasedOnDistanceWRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointment/visittype/facilities?zipCode={ZIPCODE}&distance=10&offset=1&limit=8",
		"Method=GET",
		"Snapshot=t317153.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_49_GetFacilitiesBasedOnDistanceW", LR_AUTO);
	//lr_output_message("Get Facilities based on distance W info is %s",lr_eval_string("{GetFacilitiesBasedOnDistanceWRES}"));
	lr_think_time(3);
	*/
/*
//DiagonsticMammogram W
	lr_start_transaction("STRA_SP4_50_DiagonsticMammogramW");
	web_reg_save_param("DiagonsticMammogramWRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/DiagnosticMammogram/Schedule",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t221565.inf",
		ITEMDATA,
		"Name=OrderImages", "Value=OrderImages", ENDITEM,
		"Name=InsuranceImages", "Value=InsuranceImages", ENDITEM,
		"Name=hasorderfiles", "Value=true", ENDITEM,
		"Name=imageTypeName", "Value=CT", ENDITEM,
		"Name=ImagingDays", "Value=09/09/2009", ENDITEM,
		"Name=hasinsurancefiles", "Value=true", ENDITEM,
		"Name=epicvisittypeid", "Value=2", ENDITEM,
		"Name=sentaraid", "Value=86102153", ENDITEM,
		"Name=InsuranceId", "Value=42626582", ENDITEM,
		"Name=QuestionAndAnswers[0].value", "Value=True", ENDITEM,
		"Name=appointmentdate", "Value=09072009", ENDITEM,
		"Name=additionaldetails", "Value=Heart Ache", ENDITEM,
		"Name=facility", "Value=Sentara Northern Virginia Medical Center", ENDITEM,
		"Name=QuestionAndAnswers[1].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[2].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[3].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[4].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[5].Value", "Value=NA", ENDITEM,
		"Name=ReasonForVIsit", "Value=Health issue", ENDITEM,
		"Name=MobilityCommunicationNeeds", "Value=Mobility  Communication Needs", ENDITEM,
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_50_DiagonsticMammogramW", LR_AUTO);
	lr_output_message("DiagonsticMammogramWRES info is %s",lr_eval_string("{DiagonsticMammogramWRES}"));
	lr_think_time(3);

//CTScan W
	lr_start_transaction("STRA_SP4_51_CTScanW");
	web_reg_save_param("CTScanWRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/CTScan/Schedule",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t690357.inf",
		ITEMDATA,
		"Name=OrderImages", "Value=OrderImages", ENDITEM,
		"Name=InsuranceImages", "Value=InsuranceImages", ENDITEM,
		"Name=hasorderfiles", "Value=true", ENDITEM,
		"Name=imageTypeName", "Value=CT", ENDITEM,
		"Name=ImagingDays", "Value=09/09/2009", ENDITEM,
		"Name=hasinsurancefiles", "Value=true", ENDITEM,
		"Name=epicvisittypeid", "Value=2", ENDITEM,
		"Name=sentaraid", "Value=86102153", ENDITEM,
		"Name=InsuranceId", "Value=42626582", ENDITEM,
		"Name=QuestionAndAnswers[0].Value", "Value=True", ENDITEM,
		"Name=appointmentdate", "Value=09072009", ENDITEM,
		"Name=additionaldetails", "Value=Heart Ache", ENDITEM,
		"Name=facility", "Value=Sentara Northern Virginia Medical Center", ENDITEM,
		"Name=QuestionAndAnswers[1].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[2].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[3].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[4].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[5].Value", "Value=NA", ENDITEM,
		"Name=ReasonForVIsit", "Value=Health issue", ENDITEM,
		"Name=MobilityCommunicationNeeds", "Value=Mobility  Communication Needs", ENDITEM,
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP4_51_CTScanW", LR_AUTO);
	lr_output_message("CTScanW info is %s",lr_eval_string("{CTScanWRES}"));
	lr_think_time(3);
//MRI W
	lr_start_transaction("STRA_SP4_52_MRIW");
	web_reg_save_param("MRIWRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/MRI/Schedule",
		"Method=POST",
		"EncType=multipart/form-data",
		"Snapshot=t169795.inf",
		ITEMDATA,
		"Name=OrderImages", "Value=OrderImages", ENDITEM,
		"Name=InsuranceImages", "Value=InsuranceImages", ENDITEM,
		"Name=hasorderfiles", "Value=true", ENDITEM,
		"Name=imageTypeName", "Value=CT", ENDITEM,
		"Name=ImagingDays", "Value=09/09/2009", ENDITEM,
		"Name=hasinsurancefiles", "Value=true", ENDITEM,
		"Name=epicvisittypeid", "Value=2", ENDITEM,
		"Name=sentaraid", "Value=86102153", ENDITEM,
		"Name=InsuranceId", "Value=42626582", ENDITEM,
		"Name=QuestionAndAnswers[0].Value", "Value=True", ENDITEM,
		"Name=appointmentdate", "Value=09072009", ENDITEM,
		"Name=additionaldetails", "Value=Heart Ache", ENDITEM,
		"Name=facility", "Value=Sentara Northern Virginia Medical Center", ENDITEM,
		"Name=QuestionAndAnswers[1].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[2].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[3].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[4].Value", "Value=True", ENDITEM,
		"Name=QuestionAndAnswers[5].Value", "Value=NA", ENDITEM,
		"Name=ReasonForVIsit", "Value=Health issue", ENDITEM,
		"Name=MobilityCommunicationNeeds", "Value=Mobility  Communication Needs", ENDITEM,
		HEADERS,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_52_MRIW", LR_AUTO);
	lr_output_message("MRIW info is %s",lr_eval_string("{MRIWRES}"));
	lr_think_time(3);

//Facilities with OpenSlots
	lr_start_transaction("STRA_SP4_53_FacilitiesWithOpenSlots");
	web_reg_save_param("FWithOpenSlotsRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointment/facilities",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t89655.inf",
		"Body={\n"
		"\"visitTypeId\": \"{{epicVisitTypeId}}\",\n"
		"\"zipcode\": \"{ZIPCODE}\",\n"
		"\"offset\": 4,\n"
		"\"limit\": 1,\n"
		"\"startDate\": \"10/25/2018\",\n"
		"\"endDate\": \"10/25/2018\",\n"
		"\"showNextAvailableSlots\": false\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_53_FacilitiesWithOpenSlots", LR_AUTO);
	lr_output_message("Facilities with OpenSlots info is %s",lr_eval_string("{FWithOpenSlotsRES}"));
	lr_think_time(3);

*/







/*
	web_reg_save_param_json(
       "ParamName=EPI_ID",
       "QueryString=$.sameArea.providers.[1].EPI",
       SEARCH_FILTERS,
       "Scope=Body",
       LAST);
	web_reg_save_param_json(
       "ParamName=Provider_ID",
       "QueryString=$.sameArea.providers.[1].providerId",
       SEARCH_FILTERS,
       "Scope=Body",
       LAST);
	
	//"providerId":"1631","EPI":"
	web_reg_save_param("EPI_ID",
	                   "LB=\"EPI\":\"",
	                   "RB=\"",
	                   LAST);
	                                     
	                   
	                   
*/	                   
	
//ProviderDetailsforRescheduleAppoinment API
	//lr_start_transaction("STRA_SP3_04_ProviderRescheduleAppoinment");
	//web_reg_save_param("RescheduleResponse", "LB=", "RB=",LAST);	
//	web_rest("GET: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/PatientVisits/Providers/{Provider_ID}?visittypeid=1099&idType=provider",
//		"Method=GET",
//		"Snapshot=t668675.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);

	//lr_end_transaction("STRA_SP3_04_ProviderRescheduleAppoinment", LR_AUTO);
	//lr_output_message("ProviderDetailsforrescheduleRes is %s",lr_eval_string("{RescheduleResponse}"));
	//"date":"2018-09-06"


//2D Schedule
	lr_start_transaction("STRA_SP4_54_2DSchedule");
	//web_reg_save_param("2DScheduleRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/2DMammogram/Schedule",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t927331.inf",
		"Body={ \n"
		"\"reasonId\": \"1103\", \n"
		"\"appointmentId\": \"\", \n"
		"\"date\": \"{OPENDATE}\",\n"
		"\"time\": \"{TimeStamp}\",\n"
		//"\"NPI\": \"{NPI_ID}\", \n"
		"\"NPI\": \"\", \n"
		"\"waitlist\": false, \n"
		"\"phone\": \"\", \n"
		"\"concern\": \"\", \n"
		"\"concernDetails\": \"\", \n"
		"\"commDetails\": \"\", \n"
		"\"visitType\": \"EXTERNAL\", \n"
		//"\"departmentId\": \"{Department_ID}\", \n"
		"\"departmentId\": \"20837003\", \n"
		"\"departmentType\": \"EXTERNAL\", \n"
		"\"providerType\": \"NPI\", \n"
		"\"duration\": \"{DurationTime}\"\n"
		"}\n",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_54_2DSchedule", LR_AUTO);
	//lr_output_message("2DSchedule info is %s",lr_eval_string("{2DScheduleRES}"));
	lr_think_time(3);
//3D Schedule
	lr_start_transaction("STRA_SP4_55_3DSchedule");
	//web_reg_save_param("3DScheduleRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointments/3DMammogram/Schedule",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t701473.inf",
		"Body={ \n"
		"\"reasonId\": \"1103\", \n"
		"\"appointmentId\": \"\", \n"
		"\"date\": \"{OPENDATE}\",\n"
		"\"time\": \"{TimeStamp1}\",\n"
		//"\"NPI\": \"{NPI_ID}\", \n"
		"\"NPI\": \"\", \n"
		"\"waitlist\": false, \n"
		"\"phone\": \"\", \n"
		"\"concern\": \"\", \n"
		"\"concernDetails\": \"\", \n"
		"\"commDetails\": \"\", \n"
		"\"visitType\": \"EXTERNAL\", \n"
		//"\"departmentId\": \"{Department_ID}\", \n"
		"\"departmentId\": \"20837003\", \n"
		"\"departmentType\": \"EXTERNAL\", \n"
		"\"providerType\": \"NPI\", \n"
		"\"duration\": \"{DurationTime}\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_55_3DSchedule", LR_AUTO);
	//lr_output_message("3D Schedule info is %s",lr_eval_string("{3DScheduleRES}"));
	lr_think_time(3);

	//"deviceDescription":"{DEVICE}","clientDescription":"{CLINTDES}","location":"Virginia Beac
//	web_reg_save_param("deviceDescription",
//	                   "LB=\"deviceDescription\":\"",
//	                   "RB=\",",
//	                   LAST);
//	//"clientDescription":"{CLINTDES}","location"
//	web_reg_save_param("clientDescription",
//	                   "LB=\"clientDescription\":\"",
//	                   "RB=\",",
//	                   LAST);
//Get Session Devicess
	lr_start_transaction("STRA_SP4_56_GetSessionDevicess");
	//web_reg_save_param("GetSessionDevicessRES", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/sessiondevices",
		"Method=GET",
		"Snapshot=t763765.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_56_GetSessionDevicess", LR_AUTO);
//	lr_output_message("Get Session Devicess info is %s",lr_eval_string("{GetSessionDevicessRES}"));
//	lr_output_message("deviceDescription info is %s",lr_eval_string("{deviceDescription}"));
//	lr_output_message("clientDescription info is %s",lr_eval_string("{clientDescription}"));
	lr_think_time(3);
//Post Session Devicess
	lr_start_transaction("STRA_SP4_57_PostSessionDevicess");
	//web_reg_save_param("PostSessionDevicessRES", "LB=", "RB=",LAST);
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/Profile/sessiondevices",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t931578.inf",
		"Body={\n"
		"\"sessionId\": \"{authToken_1}\",\n"
		"\"deviceDescription\": \"{DEVICE}\",\n"
		"\"clientDescription\": \"{CLINTDES}\",\n"
		"\"location\": {\n"
		"\"zipcode\": \"23462\",\n"
		"\"latitude\": \"36.8400852\",\n"
		"\"longitude\": \"-76.1493891\"\n"
		"}\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_57_PostSessionDevicess", LR_AUTO);
	//lr_output_message("Post Session Devicess info is %s",lr_eval_string("{PostSessionDevicessRES}"));

	//"testId": "2001133135"
	web_reg_save_param("TID",
	                   "LB=\"testId\":\"",
	                   "RB=\"",
	                   "convert=HTML_TO_URL",
	                    LAST);
//TestResults API	
	lr_start_transaction("STRA_SP5_02_TestResults");
	//web_reg_save_param("TestResultRes", "LB=", "RB=",LAST);		
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t12541.inf",
		"Body={\n"
		"\"sortBy\": \"Date\",\n"
		"\"sortOrder\": \"Descending\",\n"
		"\"offset\": 1,\n"
		"\"limit\": 10,\n"
		"\"searchTerm\": \"\",	\n"
		"\"showHospitalResults\": true\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP5_02_TestResults", LR_AUTO);
	//lr_output_message("Test Results info is %s",lr_eval_string("{TestResultRes}"));
	lr_output_message("Test ID info is %s",lr_eval_string("{TID}"));
		lr_think_time(5);

	//{"componentId":"67626","
	web_reg_save_param("componentId",
	                   "LB=\"componentId\":\"",
	                   "RB=\"",
	                   "ORD=ALL",
	                   LAST);

//TestResultsById
 	lr_start_transaction("STRA_SP5_03_TestResultsById");
	web_reg_save_param("TestResultsByIdRes", "LB=", "RB=",LAST);		
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults/{TID}",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t246234.inf",
		"Body={\n"
		"\"sortBy\": \"Date\",\n"
		"\"sortOrder\": \"Descending\",\n"
		"\"offset\": 1,\n"
		"\"limit\": 10,\n"
		"\"searchTerm\": \"\",  \n"
		"\"showHospitalResults\": true\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	
	lr_end_transaction("STRA_SP5_03_TestResultsById", LR_AUTO);
	lr_output_message("Test Results BY ID info is %s",lr_eval_string("{TestResultsByIdRes}"));
	//lr_output_message("componentId info is %s",lr_eval_string("{componentId}"));
		lr_think_time(5);

//TestResultsPAST	

	lr_start_transaction("STRA_SP5_04_TestResultsPAST");
	//web_reg_save_param("TestResultsPASTRes", "LB=", "RB=",LAST);		
	web_rest("POST: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/TestResults/Past",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t264196.inf",
		"Body={\n"
		"\"isGraph\":false,\n"
		"\"componentIds\": [{{COMPONENTID}}],\n"
		//"\"componentIds\": [\"51984\"],\n"
		"\"sortBy\": \"OldestFirst\",\n"
		"\"selectionType\": \"NumberofResults\",\n"
		"\"filters\": {\n"
		"\"date\": {\n"
		"\"from\": \"2017-01-01\",\n"
		"\"to\": \"2018-01-28\"\n"
		"},\n"
		"\"resultCount\":5\n"
		"}\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP5_04_TestResultsPAST", LR_AUTO);
	//lr_output_message("Test Results info is %s",lr_eval_string("{TestResultsPASTRes}"));

		lr_think_time(5);
////TestResultsTrend
//	lr_start_transaction("STRA_SP5_05_TestResultsTrend");
//	//web_reg_save_param("TestResultsTrendRes", "LB=", "RB=",LAST);		
//	web_rest("POST: {UATURL}/consumer/v1/Pa...",
//		"URL={UATURL}/consumer/v1/Patient/TestResults/Trend",
//		"Method=POST",
//		"EncType=raw",
//		"Snapshot=t31802.inf",
//		"Body={\n"
//		"\"componentIds\": [{COMPONENTID}],\n"
//		"\"sortBy\": \"OldestFirst\",\n"
//		"\"selectionType\": \"NumberOfResults\",\n"
//		"\"filters\": {\n"
//		"\"date\": {\n"
//		"\"from\": \"2017-01-01\",\n"
//		"\"to\": \"2018-02-28\"\n"
//		"},\n"
//		"\"resultCount\":5\n"
//		"}\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//	lr_end_transaction("STRA_SP5_05_TestResultsTrend", LR_AUTO);
//	//lr_output_message("Test Results trend info is %s",lr_eval_string("{TestResultsTrendRes}"));
//		lr_think_time(5);

	//"token": "olj4nBAEh3mT6nnCElNx9zn69wMsm18xMYyMpOUi2vGB2p5l0fHJBE3b8tr1oITpmH/eSrSuWir6dYCk3JY1lQ=="
	web_reg_save_param("Auth2",
	                   "LB=\"token\":\"",
	                   "RB=\"",
	                   "convert=HTML_TO_URL",
	                   LAST);
//LucySSO
	lr_start_transaction("STRA_SP5_06_LucySSO");
	//web_reg_save_param("LucySSORes", "LB=", "RB=",LAST);		
	web_rest("GET: {UATURL}/common/v1/Char...",
		"URL={UATURL}/common/v1/Chart/lucysso/68280885E0D14152A5BE6E1D96D011F7",
		"Method=GET",
		"Snapshot=t138743.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP5_06_LucySSO", LR_AUTO);
	//lr_output_message("LucySSO info is %s",lr_eval_string("{LucySSORes}"));
	lr_output_message("LucySSO AuthToken info is %s",lr_eval_string("{Auth2}"));
		lr_think_time(5);
////LucySSO_AuthorizedToken
//	lr_start_transaction("STRA_SP5_07_LucySSOAuthorizedToken");
//	//web_reg_save_param("LucySSOAuthorizedTokenRes", "LB=", "RB=",LAST);		
//	web_rest("GET: {UATURL}/common/v1/User...",
//		"URL={UATURL}/common/v1/User/sso?sso={Auth2}",
//		"Method=GET",
//		"Snapshot=t385396.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);

	
//	lr_end_transaction("STRA_SP5_07_LucySSOAuthorizedToken", LR_AUTO);
//	//lr_output_message("Lusy SSO Auth Token info is %s",lr_eval_string("{LucySSOAuthorizedTokenRes}"));
lr_start_transaction("STRA_SP5_07_LucySSOAuthorizedToken");

	web_rest("POST: {UATURL}/common/v1/Chart/sso",
		"URL={UATURL}/common/v1/Chart/sso",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t139507.inf",
		"Body={\n"
		"\"token\": \"{Auth2}\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);


lr_end_transaction("STRA_SP5_07_LucySSOAuthorizedToken", LR_AUTO);
		lr_think_time(5);
/***********************SPRINT6****************************************/

//Beacon
	lr_start_transaction("STRA_SP6_08_Beacon");
	//web_reg_save_param("BeaconRes", "LB=", "RB=",LAST);		
	web_rest("GET: {UATURL}/common/v1/Pati...",
		"URL={UATURL}/common/v1/Patient/PatientInformation?deviceId=AA_AA_AA_AA_AAAE",
		"Method=GET",
		"Snapshot=t706111.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP6_08_Beacon", LR_AUTO);
	//lr_output_message("Beacon info is %s",lr_eval_string("{BeaconRes}"));
		lr_think_time(5);
//EcheckinPendingAppointments
	lr_start_transaction("STRA_SP6_09_EcheckinPendingAppointments");
	//web_reg_save_param("EcheckPendAppointRes", "LB=", "RB=",LAST);		
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/EcheckinPendingAppointments",
		"Method=GET",
		"Snapshot=t257779.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP6_09_EcheckinPendingAppointments", LR_AUTO);
	//lr_output_message(" Echecking Pending Appointments info is %s",lr_eval_string("{EcheckPendAppointRes}"));
		lr_think_time(5);
//IconBadge
	lr_start_transaction("STRA_SP6_10_IconBadge");
	//web_reg_save_param("IconBadgeRes", "LB=", "RB=",LAST);		
	web_rest("GET: {UATURL}/consumer/v1/Pa...",
		"URL={UATURL}/consumer/v1/Patient/PatientDashboard",
		"Method=GET",
		"Snapshot=t763865.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_SP6_10_IconBadge", LR_AUTO);
	lr_output_message("IconBadge info is %s",lr_eval_string("{IconBadgeRes}"));
//Unauthenticated 
//App Upgraqde
lr_start_transaction("STRA_UAT_APPUPGRADE");

	web_rest("POST: {UATURL}/common/v1/Device/AppUpgrade",
		"URL={UATURL}/common/v1/Device/AppUpgrade",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t347820.inf",
		"Body={\n"
		"\"appName\": \"sentaraios\",\n"
		"\"version\": \"2.3\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

lr_end_transaction("STRA_UAT_APPUPGRADE", LR_AUTO);
//{"facilities":[{"facilityId":37760,"facilityName"
	web_reg_save_param("FacilityID",
	                   "LB=\"facilityId\":",
	                   "RB=,\"",
	                    LAST);
//This API is used to get facilities based on locationtype, distance and zipcode
lr_start_transaction("STRA_UAT_FACILITIES");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities?zipCode=23455&locationType=3&distance=26",
		"URL={UATURL}/common/v1/PatientVisits/Facilities?zipCode=23455&locationType=3&distance=26",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_FACILITIES", LR_AUTO);
//This API is used to get facility details based on facility.
lr_start_transaction("STRA_SP3_29_FacilityDetails");
	//web_reg_save_param("FinfFacilityRes", "LB=", "RB=",LAST);
	web_rest("GET: {UATURL}/common/v1/Pa...",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/{FacilityID}",
		"Method=GET",
		"Snapshot=t718694.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);	
	lr_end_transaction("STRA_SP3_29_FacilityDetails", LR_AUTO);
//This API is used to get list of facilities for campus maps.
lr_start_transaction("STRA_UAT_CampusMapFACILITIES");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities/CampusMap/Facilities",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/CampusMap/Facilities",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_CampusMapFACILITIES", LR_AUTO);
//This API is used for system init check.
//Not Used in ALL Platforms
//lr_start_transaction("STRA_UAT_Init");
//	web_rest("GET: {UATURL}/common/v1/Init?1",
//		"URL={UATURL}/common/v1/Init?1",
//		"Method=GET",
//		"Snapshot=t972900.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//lr_end_transaction("STRA_UAT_Init", LR_AUTO);
//This is used to get List of language.
lr_start_transaction("STRA_UAT_Languages");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Languages",
		"URL={UATURL}/common/v1/PatientVisits/Languages",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_Languages", LR_AUTO);
//This API is used to get list of Mdlive states.
lr_start_transaction("STRA_UAT_MdliveStates");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Mdlive/States",
		"URL={UATURL}/common/v1/PatientVisits/Mdlive/States",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_MdliveStates", LR_AUTO);
//This API is used to retrieve patient information.
//Not used in ALL platforms
//lr_start_transaction("STRA_UAT_patientInfo");
//	web_rest("GET: {UATURL}/common/v1/Patient/PatientInformation",
//		"URL={UATURL}/common/v1/Patient/PatientInformation",
//		"Method=GET",
//		"Snapshot=t972900.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//lr_end_transaction("STRA_UAT_patientInfo", LR_AUTO);
//This API is used to get provider image
lr_start_transaction("STRA_UAT_ProviderImage");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/7832/Image",
		"URL={UATURL}/common/v1/PatientVisits/Providers/7832/Image",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_ProviderImage", LR_AUTO);
//This API is used to get visit reason
lr_start_transaction("STRA_UAT_GetVisitReason");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"URL={UATURL}/common/v1/PatientVisits/Providers/VisitType/New/Reasons",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetVisitReason", LR_AUTO);
//This API is used to get specialities
lr_start_transaction("STRA_UAT_GetSpecialties");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"URL={UATURL}/common/v1/PatientVisits/Providers/Specialties?searchTerm=&GetCareType=2",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetSpecialties", LR_AUTO);
//This API is used for reviewing comments and ratings
lr_start_transaction("STRA_UAT_GetReviewComments");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/{NPI}/ReviewComments",
		"URL={UATURL}/common/v1/PatientVisits/Providers/{NPI}/ReviewComments",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetReviewComments", LR_AUTO);
//This API is used to get list of questions
lr_start_transaction("STRA_UAT_GetListQuestions");
	web_rest("GET: {UATURL}/common/v1/Questions",
		"URL={UATURL}/common/v1/Questions",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetListQuestions", LR_AUTO);
//This API is used to validate token

//This API calls under triggered email for reset password., hence, Commented in scripting
//lr_start_transaction("STRA_UAT_Token");
//
//	web_rest("POST: {UATURL}/common/v1/User/token",
//		"URL={UATURL}/common/v1/User/token",
//		"Method=POST",
//		"EncType=raw",
//		"Snapshot=t571149.inf",
//		"Body={\n"
//		"\"encryptedValue\": \"string\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//lr_end_transaction("STRA_UAT_Token", LR_AUTO);
//This API is used to retrieve Login information using SSO token
//Not Used in all platforms
//lr_start_transaction("STRA_UAT_LoginInfoSoToken");
//	web_rest("GET: {UATURL}/common/v1/User/sso",
//		"URL={UATURL}/common/v1/User/sso",
//		"Method=GET",
//		"Snapshot=t972900.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//lr_end_transaction("STRA_UAT_LoginInfoSoToken", LR_AUTO);
//This API is used to validate userid.
lr_start_transaction("STRA_UAT_ValidateUserID");
	web_rest("GET: {UATURL}/common/v1/Validation/UserId/{userid}",
		"URL={UATURL}/common/v1/Validation/UserId/{userid}",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_ValidateUserID", LR_AUTO);
//This API is used to validate username.
lr_start_transaction("STRA_UAT_ValidateUserName");
	web_rest("GET: {UATURL}/common/v1/Validation/Username/{username}",
		"URL={UATURL}/common/v1/Validation/Username/{username}",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_ValidateUserName", LR_AUTO);
//This API is used to get Location Type.
lr_start_transaction("STRA_UAT_GetLocationType");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Facilities/LocationTypes",
		"URL={UATURL}/common/v1/PatientVisits/Facilities/LocationTypes",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetLocationType", LR_AUTO);
//This API not using in app.Hence,commented the script
//This API is used to get appointments whoes eChecking is pending
//lr_start_transaction("STRA_UAT_GetPatientEcheckinPendingAppointments");
//	web_rest("GET: {UATURL}/common/v1/Patient/EcheckinPendingAppointments",
//		"URL={UATURL}/common/v1/Patient/EcheckinPendingAppointments",
//		"Method=GET",
//		"Snapshot=t972900.inf",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//lr_end_transaction("STRA_UAT_GetPatientEcheckinPendingAppointments", LR_AUTO);
//This API is used to get provider details based on visittype and providerS
lr_start_transaction("STRA_UAT_GetProviderDetailsOnVtypeAndProviders");
	web_rest("GET: {UATURL}/common/v1/PatientVisits/Providers/66?idType=epi",
		"URL={UATURL}/common/v1/PatientVisits/Providers/66?idType=epi",
		"Method=GET",
		"Snapshot=t972900.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
lr_end_transaction("STRA_UAT_GetProviderDetailsOnVtypeAndProviders", LR_AUTO);
//This API ios used to get Providers
lr_start_transaction("STRA_UAT_GetProviders");

	web_rest("POST: {UATURL}/common/v1/PatientVisits/Providers",
		"URL={UATURL}/common/v1/PatientVisits/Providers",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t689317.inf",
		"Body={\n"
		"\"time\": {\n"
		"\"from\": \"\",\n"
		"\"to\": \"\"\n"
		"},\n"
		"\"reasonId\": \"\",\n"
		"\"date\": \"\",\n"
		"\"providerName\": \"\",\n"
		"\"providerType\": \"\",\n"
		"\"gender\": \"\",\n"
		"\"zipcode\": \"{ZIPCODE}\",\n"
		"\"latitude\": \"\",\n"
		"\"longitude\": \"\",\n"
		"\"radius\": 10,\n"
		"\"languages\": \"\",\n"
		"\"specialty\": \"\",\n"
		"\"offset\":\"1\",\n"
		"\"limit\":\"40\",\n"
		"\"showPrimaryCareProviders\": \"true\",\n"
		"\"onlyShowProvidersWhoScheduleOnline\": \"true\",\n"
		"\"showNonSentaraProviders\":\"true\",\n"
		"\"smg\":0\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetProviders", LR_AUTO);

//This API password reset purpose,hence, commented this API
//This API is used to validate password.
//lr_start_transaction("STRA_UAT_ValidationPassword");
//
//	web_rest("POST: {UATURL}/common/v1/Validation/Password",
//		"URL={UATURL}/common/v1/Validation/Password",
//		"Method=POST",
//		"EncType=raw",
//		"Snapshot=t785465.inf",
//		"Body={\n"
//		"\"userId\": \"string\",\n"
//		"\"firstName\": \"string\",\n"
//		"\"lastName\": \"string\",\n"
//		"\"passwordNew\": \"string\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//
//	
//lr_end_transaction("STRA_UAT_ValidationPassword", LR_AUTO);
//This API is used to validate identity of user.
//Unic data required for this API.Hence,Commented the APi
//lr_start_transaction("STRA_UAT_MdliveStates");
//
//	web_rest("POST: {UATURL}/common/v1/Validation/Identity",
//		"URL={UATURL}/common/v1/Validation/Identity",
//		"Method=POST",
//		"EncType=raw",
//		"Snapshot=t545002.inf",
//		"Body={\n"
//		"\"user\": {\n"
//		"\"firstName\": \"string\",\n"
//		"\"lastName\": \"string\",\n"
//		"\"dob\": \"2019-10-23T07:08:22.360Z\",\n"
//		"\"email\": \"string\",\n"
//		"\"ssn\": \"string\",\n"
//		"\"age\": \"string\",\n"
//		"\"gender\": \"string\"\n"
//		"},\n"
//		"\"credentials\": {\n"
//		"\"userId\": \"string\",\n"
//		"\"password\": \"string\"\n"
//		"},\n"
//		"\"questions\": [\n"
//		"{\n"
//		"\"question\": \"string\",\n"
//		"\"answer\": \"string\"\n"
//		"}\n"
//		"],\n"
//		"\"activationCode\": \"string\",\n"
//		"\"terms\": 0,\n"
//		"\"channel\": \"string\",\n"
//		"\"language\": \"string\"\n"
//		"}",
//		HEADERS,
//		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
//		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
//		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
//		LAST);
//lr_end_transaction("STRA_UAT_MdliveStates", LR_AUTO);


//This API is used to retrieve details of appointment
	lr_start_transaction("STRA_UAT_retriveAppointmentDetail");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Appointments/2L23iQ%252Bz1Lphy4CzjWSLvw%253D%253D?appointmentType=1",
		"URL={UATURL}/consumer/v1/PatientVisits/Appointments/2L23iQ%252Bz1Lphy4CzjWSLvw%253D%253D?appointmentType=1",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_retriveAppointmentDetail", LR_AUTO);
//This API is used to get SSO token	
	lr_start_transaction("STRA_UAT_GetSSOTOKEN");
	web_rest("GET: {UATURL}/consumer/v1/Chart/sso",
		"URL={UATURL}/consumer/v1/Chart/sso",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetSSOTOKEN", LR_AUTO);
//This API is used to get configuration for virtual care
	lr_start_transaction("STRA_UAT_GetVirtualCare");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Configurations/VirtualCare",
		"URL={UATURL}/consumer/v1/PatientVisits/Configurations/VirtualCare",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetVirtualCare", LR_AUTO);

//This API is used to get Facilities of Imaging
	lr_start_transaction("STRA_UAT_GetImagingFacilities");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/imaging/appointment/facilities?visitTypeId=15800&zipCode=23455&distance=5&offset=1&limit=10&imagingType=3&uniqueFacilities=true",
		"URL={UATURL}/consumer/v1/PatientVisits/imaging/appointment/facilities?visitTypeId=15800&zipCode=23455&distance=5&offset=1&limit=10&imagingType=3&uniqueFacilities=true",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetImagingFacilities", LR_AUTO);
//This API is used to get list of languages
	lr_start_transaction("STRA_UAT_GetListOfLanguages");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Languages",
		"URL={UATURL}/consumer/v1/PatientVisits/Languages",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetListOfLanguages", LR_AUTO);
//This API is used to get SSO Token fo Mdlive
	lr_start_transaction("STRA_UAT_SSOTokForMdlive");
	
	web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Mdlive/SSO",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/SSO",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t312219.inf",
		"Body={\n"
		"\"auth\": {\n"
		"\"first_name\": \"string\",\n"
		"\"last_name\": \"string\",\n"
		"\"gender\": \"string\",\n"
		"\"birthdate\": \"string\",\n"
		"\"subscriber_id\": \"string\",\n"
		"\"member_id\": \"string\",\n"
		"\"phone\": \"string\",\n"
		"\"email\": \"string\",\n"
		"\"address1\": \"string\",\n"
		"\"city\": \"string\",\n"
		"\"state\": \"string\",\n"
		"\"zip\": \"string\",\n"
		"\"relationship\": \"self\",\n"
		"\"primary_first_name\": \"string\",\n"
		"\"primary_last_name\": \"string\",\n"
		"\"primary_gender\": \"string\",\n"
		"\"primary_birthdate\": \"string\",\n"
		"\"primary_subscriber_id\": \"string\",\n"
		"\"primary_member_id\": \"string\",\n"
		"\"primary_address1\": \"string\",\n"
		"\"primary_address2\": \"string\",\n"
		"\"primary_city\": \"string\",\n"
		"\"primary_state\": \"string\",\n"
		"\"primary_zip\": \"string\"\n"
		"},\n"
		"\"org\": {\n"
		"\"ou\": \"1\"\n"
		"},\n"
		"\"api\": {\n"
		"\"api_key\": \"string\",\n"
		"\"password\": \"string\"\n"
		"}\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	
	lr_end_transaction("STRA_UAT_SSOTokForMdlive", LR_AUTO);
//This API is used to extend SSO token for Mdlive	
	lr_start_transaction("STRA_UAT_ExtendSSOTokForMdlive");
	web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Mdlive/E...",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/ExtendedSSO?ljsjgdflh=",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t993927.inf",
		"Body={\n"
		"\"auth\": \"string\",\n"
		"\"org\": {\n"
		"\"ou\": \"string\"\n"
		"},\n"
		"\"api\": {\n"
		"\"api_key\": \"string\",\n"
		"\"password\": \"string\"\n"
		"}\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_ExtendSSOTokForMdlive", LR_AUTO);
//This API is used to get list of OUInsurance	
	lr_start_transaction("STRA_UAT_OUInsuranceList");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Mdlive/OUInsurance",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/OUInsurance",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_OUInsuranceList", LR_AUTO);
//This API is used to get list of insurance of the person	
	lr_start_transaction("STRA_UAT_PersonInsurancesList");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Mdlive/PersonOUInsurance",
		"URL={UATURL}/consumer/v1/PatientVisits/Mdlive/PersonOUInsurance",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_PersonInsurancesList", LR_AUTO);
//This API is used to returns total count of alerts
	lr_start_transaction("STRA_UAT_AlertsCount");
	web_rest("GET: {UATURL}/consumer/v1/Patient/messages/alerts/count",
		"URL={UATURL}/consumer/v1/Patient/messages/alerts/count",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_AlertsCount", LR_AUTO);
//This API is used to send message
	lr_start_transaction("STRA_UAT_SendMessage");
	
		web_rest("POST: {UATURL}/consumer/v1/Patient/SendMessage",
		"URL={UATURL}/consumer/v1/Patient/SendMessage",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t156249.inf",
		"Body={\n"
		"\"body\": \"string\",\n"
		"\"messageType\": \"string\",\n"
		"\"subject\": \"string\",\n"
		"\"sendTo\": \"string\",\n"
		"\"viewers\": [\n"
		"{\n"
		"\"viewer\": \"string\"\n"
		"}\n"
		"]\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	
	lr_end_transaction("STRA_UAT_SendMessage", LR_AUTO);
//This API is used get Medical History of the patient	
	lr_start_transaction("STRA_UAT_PatientMedHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/MedicalHistory",
		"URL={UATURL}/consumer/v1/Patient/MedicalHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_PatientMedHistory", LR_AUTO);
//This API is used get Surgical History of the patient	
	lr_start_transaction("STRA_UAT_PatientSurgeHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/SurgicalHistory",
		"URL={UATURL}/consumer/v1/Patient/SurgicalHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_PatientSurgeHistory", LR_AUTO);
//This API is used get Family History of the patient	
	lr_start_transaction("STRA_UAT_PatientFamilyHistory");
	web_rest("GET: {UATURL}/consumer/v1/Patient/FamilyHistory",
		"URL={UATURL}/consumer/v1/Patient/FamilyHistory",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_PatientFamilyHistory", LR_AUTO);
//This API is used to get gender information of the patient	
	lr_start_transaction("STRA_UAT_PatientGenderInfo");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Gender",
		"URL={UATURL}/consumer/v1/Patient/Gender",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_PatientGenderInfo", LR_AUTO);
//This API is used to send email
		lr_start_transaction("STRA_UAT_SendEmail");
		web_rest("PUT: {UATURL}/consumer/v1/Patient/Profile/email",
		"URL={UATURL}/consumer/v1/Patient/Profile/email",
		"Method=PUT",
		"EncType=raw",
		"Snapshot=t376394.inf",
		"Body={\n"
		"\"userEmail\": {\n"
		"\"email\": \"string\"\n"
		"},\n"
		"\"channel\": \"string\",\n"
		"\"lang\": \"string\"\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	web_rest("GET: {UATURL}/consumer/v1/Patient/Gender",
		"URL={UATURL}/consumer/v1/Patient/Gender",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_SendEmail", LR_AUTO);
//This API is used to get Languages	
		lr_start_transaction("STRA_UAT_GetLanguages");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Profile/Languages",
		"URL={UATURL}/consumer/v1/Patient/Profile/Languages",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetLanguages", LR_AUTO);
//This API is used to get entitlements of user.	
		lr_start_transaction("STRA_UAT_GetEntitlementsOfUser");
	web_rest("GET: {UATURL}/consumer/v1/Patient/Profile/entitlements",
		"URL={UATURL}/consumer/v1/Patient/Profile/entitlements",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetEntitlementsOfUser", LR_AUTO);
//This API is used to get open slots for Imaging appointment with slots roll up	
		lr_start_transaction("STRA_UAT_GetOpenSlotsForImagingAppointmentSlotsrollUp");
		web_rest("POST: {UATURL}/consumer/v1/PatientVisits/Provider...",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/Imaging/OpenSlots",
		"Method=POST",
		"EncType=raw",
		"Snapshot=t18998.inf",
		"Body={\n"
		"\"visitTypeId\": \"15800\",\n"
		"\"visitType\": \"External\",\n"
		"\"departmentId\": \"20316002\",\n"
		"\"departmentType\": \"External\",\n"
		"\"startDate\": \"{START_DATE}\",\n"
		"\"endDate\": \"{END_DATE}\",\n"
		"\"showNextAvailableSlots\": true,\n"
		"\"distance\": 50\n"
		"}",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);
	
	lr_end_transaction("STRA_UAT_GetOpenSlotsForImagingAppointmentSlotsrollUp", LR_AUTO);
//This API is used to get name of the providers	

		lr_start_transaction("STRA_UAT_GetNameOfProviders");
	web_rest("GET: {UATURL}/consumer/v1/PatientVisits/Providers/name?providername=rav&offset=1&limit=50&GetCareType=2",
		"URL={UATURL}/consumer/v1/PatientVisits/Providers/name?providername=rav&offset=1&limit=50&GetCareType=2",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_GetNameOfProviders", LR_AUTO);
//This API is used to get meeting information based on appointment	
		lr_start_transaction("STRA_UAT_ZoomMeetingInfoBasedOnAppointment");
	web_rest("GET: {UATURL}/consumer/v1/Zoom/MeetingInfo/91UV8%2BA6ce0IDQWrcyj%2BRQ%3D%3D",
		"URL={UATURL}/consumer/v1/Zoom/MeetingInfo/91UV8%2BA6ce0IDQWrcyj%2BRQ%3D%3D",
		"Method=GET",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_UAT_ZoomMeetingInfoBasedOnAppointment", LR_AUTO);
//Logout 
	lr_start_transaction("STRA_SP4_8_Logout");
	web_rest("DELETE: {UATURL}/consumer/v1/User/logout",
		"URL={UATURL}/consumer/v1/User/logout",
		"Method=DELETE",
		"EncType=raw",
		"Snapshot=t792925.inf",
		HEADERS,
		"Name=Content-Type", "Value=application/json-patch+json", ENDHEADER,
		"Name=Ocp-Apim-Subscription-Key", "Value={SUBKEY}", ENDHEADER,
		"Name=Authorization", "Value={authToken_1}", ENDHEADER,
		LAST);

	lr_end_transaction("STRA_SP4_8_Logout", LR_AUTO);

		return 0;
}
